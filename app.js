require('dotenv').config();
require('module-alias/register');

const express = require('express'),
      socketIO = require('socket.io'),
      passport = require('passport'),
      mongoose = require('mongoose'),
      LocalStrategy = require('passport-local').Strategy,
      BearerStrategy = require('passport-http-bearer').Strategy,
      deviantART = require('passport-deviantart').Strategy,
      bcrypt = require('bcrypt-nodejs');

const app = express();
const port = process.env.HTTP_PORT || 3000;

const session = require('express-session'),
      RedisStore = require('connect-redis')(session),
      bodyParser = require('body-parser'),
      cookieParser = require('cookie-parser');

const { APP_URI } = require('./config/app').constants,
      routes = require('./config/routes'),
      database = require('./config/database');

const quickCatchEventsCron = require('./controllers/quickCatchEvents/cron');
const userDeleteCron = require('./controllers/users/cron');

mongoose.connect(database.mongodb.uri);

/*
  CONFIG
*/

app.use(bodyParser.json())
app.use(cookieParser())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static('public'));

app.use(function(req, res, next) {
    req.url = req.url.replace('/engine', '');
  next();
});

/*
  SESSION
*/

const User = require('@/models/user.js');

passport.use(new LocalStrategy(
  {
    usernameField: 'username',
    passwordField: 'password'
  },
  (username, password, done) => {
    const u = User.findOne({ username: new RegExp(`^${username}$`, 'i') }).then((u) => {
      console.log(u)
      if (!u) { return done(null, false); }
      if (!u.password || !bcrypt.compareSync(password, u.password)) { return done(null, false); }

      return done(null, u);
    })
  }
));

passport.use(new BearerStrategy(
  function(token, done) {
    User.findOne({ token }, function (err, user) {
      if (err) { return done(err); }
      if (!user) { return done(null, false); }
      return done(null, user, { scope: 'all' });
    });
  }
));

passport.use('deviantart', new deviantART({
    clientID: '4092',
    clientSecret: 'e12d21e929cb185ea4061461eb77a890',
    callbackURL: `${APP_URI}/engine/auth/provider/deviantart/callback`
  },
  function(token, tokenSecret, profile, done){
    const u = User.findOne({ dA: new RegExp(`^${profile.username}$`, 'i') }).then((u) => {
      if (!u) { return done(null, false); }

      return done(null, u);
    })
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User
      .findById(id)
      .populate('characters')
      .populate('news')
      .populate({
        path: 'inventory',
        populate: {
          path: 'item'
        },
      })
      .populate({
        path: 'companions',
        select: ['_id', 'name', 'trade', 'user'],
        populate: {
          path: 'companion colorStatuses pokeball'
        },
      })
      .then(function(user, err) {
        done(null, user);
    });
})

app.use(session({
  secret: 'r(Y9#Xak?>G2\c8m',
  store: new RedisStore(database.redis),
  maxAge: 360*5,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }
}))

app.use(passport.initialize())
app.use(passport.session())

/*
  ROUTES
*/

routes.forEach((route) => {
  const [method, path] = route[0].split(' ')

  if(route.length === 3) {
    app[method](path, route[1], route[2])
  } else {
    app[method](path, route[1])
  }
})

/*
  SERVER
*/

const server = app.listen(port, () => {
  console.log('\x1b[7m\x1b[40m\x1b[36m%s\x1b[0m', '🐕 Sniff sniff? What is that?');
  console.log('\x1b[7m\x1b[5m\x1b[40m\x1b[33m%s\x1b[1m%s\x1b[0m', `🐩 It is Port `, `${port} 🚀💥 ` );
})

/*
  SOCKET.IO
*/

const io = socketIO(server);
app.set('io', io);

/*
  CRONJOBS
*/
quickCatchEventsCron();
userDeleteCron();


// User.find().then(async (result) => {
//   for(let user of result) {
//     console.log(user)
//     if(!user.settings) {
//       user.settings = {
//         emailAnnouncements: true,
//         emailMentions: false,
//         emailSubscribedThread: false,
//         emailSubmissionComments: false,
//         emailPMs: true,
//       };
//
//       await user.save()
//     }
//
//   }
// })
