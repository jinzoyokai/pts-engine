module.exports.mongodb = {
  uri: process.env.MONGODB_URI || 'mongodb://localhost/pts'
}

module.exports.redis = {
  host: process.env.REDIS_HOST || 'localhost',
  port: process.env.REDIS_PORT || 6379,
  user: process.env.REDIS_USER || 'root'
}
