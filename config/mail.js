module.exports.settings = {
  api_key: process.env.MAIL_API_KEY,
  from: process.env.MAIL_FROM,
  from_name: process.env.MAIL_FROM_NAME
}
