module.exports.allowedFiletypes = [
  'png',
  'jpg',
  'jpeg',
  'gif',
  'psd',
  'clip',
  'mp3',
  'mp4',
  'webm',
  'mov',
  'avi'
];

module.exports.s3 = {
  config: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_REGION || 'us-east-1',
  },
  bucket: process.env.AWS_BUCKET || 'treasury-2',
}
