const { allowedFiletypes, s3: s3Storage } = require('@/config/storage.js');

const aws = require('aws-sdk'),
      crypto = require('crypto'),
      passport = require('passport'),
      multer = require('multer'),
      multerS3 = require('multer-s3'),
      auth = require('@/middlewares/auth.js'),
      hasPermissions = require('@/middlewares/hasPermissions.js'),
      hasAtLeastOnePermission = require('@/middlewares/hasAtLeastOnePermission.js');

const s3 = new aws.S3(s3Storage.config);

const storage = multerS3({
  s3: s3,
  acl: 'public-read',
  bucket: s3Storage.bucket,
  metadata: function (req, file, cb) {
    cb(null, {fieldName: file.fieldname});
  },
  key: function (req, file, cb) {
    fileExtension = file.originalname.split('.')[1] // get file extension from original file name

    cb(null, `${Date.now().toString()}.${fileExtension}`)
  },
})

const upload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    // if the file extension is in our accepted list
    if (allowedFiletypes.some(ext => file.originalname.endsWith('.' + ext))) {
      return cb(null, true);
    }

    // otherwise, return error
    return cb(new Error(`Only ${allowedFiletypes.join(', ')} files are allowed!`));
  }
})

const controllerPath = '@/controllers';
const API_PREFIX = '/api';

module.exports = [
  [
    'post /auth/login',
    passport.authenticate('local'),
    require(`${controllerPath}/auth/login`)
  ],
  [
    'get /auth/logout',
    [auth],
    require(`${controllerPath}/auth/logout`)
  ],
  [
    'get /auth/self',
    [auth],
    require(`${controllerPath}/users/self`)
  ],
  [
    `get ${API_PREFIX}/auth/self`,
    passport.authenticate('bearer'),
    require(`${controllerPath}/users/self`)
  ],
  [
    'get /auth/provider/deviantart/init',
    require(`${controllerPath}/auth/deviantart/init`)
  ],
  [
    'get /auth/provider/deviantart/redirect',
    passport.authenticate('deviantart')
  ],
  [
    'get /auth/provider/deviantart/callback',
    passport.authenticate('deviantart'),
    require(`${controllerPath}/auth/deviantart/callback`)
  ],
  [
    'post /upload',
    [auth, upload.single('file')],
    require(`${controllerPath}/files/upload`)
  ],
  [
  'get /seed',
    require(`${controllerPath}/seed`)
  ],
  [
  'get /refresh_thumbnails',
    require(`${controllerPath}/refreshThumbnails`)
  ],

  /*
    USERS
  */

  [
    'get /users',
    require(`${controllerPath}/users/all`)
  ],
  [
    `get ${API_PREFIX}/users`,
    [passport.authenticate('bearer'), hasAtLeastOnePermission(['can_edit_users', 'can_edit_characters'])],
    require(`${controllerPath}/users/all`)
  ],
  [
    'get /users/staff',
    [auth, hasPermissions(['can_edit_users'])],
    require(`${controllerPath}/users/staff`)
  ],
  [
    'get /users/search',
    [auth],
    require(`${controllerPath}/users/search`)
  ],
  [
    'get /users/:id',
    [auth],
    require(`${controllerPath}/users/get`)
  ],
  [
    'get /users/by_username/:username',
    [auth],
    require(`${controllerPath}/users/get_by_username`)
  ],
  [
    'post /users',
    [auth, upload.single('icon'), hasPermissions(['can_edit_users'])],
    require(`${controllerPath}/users/create`)
  ],
  [
    'patch /users/:id',
    [auth, upload.single('icon')],
    require(`${controllerPath}/users/update`)
  ],
  [
    'post /users/mass_update',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/users/massUpdate`)
  ],
  [
    'post /users/:id/restore',
    [auth, hasPermissions(['can_edit_users'])],
    require(`${controllerPath}/users/restore`)
  ],
  [
    'delete /users/:id',
    [auth, hasPermissions(['can_edit_users'])],
    require(`${controllerPath}/users/delete`)
  ],


  /*
    CONTENT
  */

  [
    'get /content',
    [auth, hasPermissions(['author'])],
    require(`${controllerPath}/content/all`)
  ],
  [
    'get /activity',
    [auth],
    require(`${controllerPath}/content/activity`)
  ],
  [
    'get /activity_items',
    require(`${controllerPath}/content/activity_items`)
  ],
  [
    'get /news',
    require(`${controllerPath}/content/news`)
  ],
  [
    'get /content/id/:id',
    require(`${controllerPath}/content/get_by_id`)
  ],
  [
    'get /content/id/:id/comments',
    require(`${controllerPath}/comments/get`)
  ],
  [
    'get /content/:slug',
    require(`${controllerPath}/content/get_by_slug`)
  ],
  [
    'post /content',
    [auth, hasPermissions(['author'])],
    require(`${controllerPath}/content/create`)
  ],
  [
    'patch /content/:id',
    [auth, hasPermissions(['author'])],
    require(`${controllerPath}/content/update`)
  ],
  [
    'delete /content/:id',
    [auth, hasPermissions(['author'])],
    require(`${controllerPath}/content/delete`)
  ],

  /*
    COMMENTS
  */

  [
    'get /comments',
    require(`${controllerPath}/comments/all`)
  ],
  [
    'post /comments',
    [auth],
    require(`${controllerPath}/comments/create`)
  ],
  [
    'patch /comments/:id',
    [auth],
    require(`${controllerPath}/comments/update`)
  ],
  [
    'delete /comments/:id',
    [auth],
    require(`${controllerPath}/comments/delete`)
  ],

  /*
    COMPANIONS
  */

  [
    'get /companions',
    [auth],
    require(`${controllerPath}/companions/all`)
  ],
  [
    'get /companions/:id',
    require(`${controllerPath}/companions/get`)
  ],
  [
    'post /companions',
    [auth, upload.single('icon'), hasPermissions(['can_edit_companions'])],
    require(`${controllerPath}/companions/create`)
  ],
  [
    'patch /companions/:id',
    [auth, upload.single('icon'), hasPermissions(['can_edit_companions'])],
    require(`${controllerPath}/companions/update`)
  ],
  [
    'delete /companions/:id',
    [auth, hasPermissions(['can_edit_companions'])],
    require(`${controllerPath}/companions/delete`)
  ],

    /*
      SUBMISSIONS
    */

    [
      'get /submissions',
      [auth],
      require(`${controllerPath}/submissions/all`)
    ],
    [
      'get /submissions/:id/comments',
      [auth],
      require(`${controllerPath}/comments/submissions`)
    ],
    [
      'get /submissions/moderate',
      [auth, hasPermissions(['can_edit_submissions'])],
      require(`${controllerPath}/submissions/moderate`)
    ],
    [
      'post /submissions',
      [auth, upload.single('image')],
      require(`${controllerPath}/submissions/create`)
    ],
    [
      'patch /submissions/:id',
      [auth],
      require(`${controllerPath}/submissions/update`)
    ],
    [
      'delete /submissions/:id',
      [auth],
      require(`${controllerPath}/submissions/delete`)
    ],

  /*
    CHARACTERS
  */

  [
    'get /characters',
    [auth, hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/characters/all`)
  ],
  [
    'get /characters/:id',
    require(`${controllerPath}/characters/get`)
  ],
  [
    'post /characters',
    [auth, upload.fields([
      { name: 'profileImage', maxCount: 1 },
      { name: 'design', maxCount: 1 }
    ]), hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/characters/create`)
  ],
  [
    'patch /characters/:id',
    [auth, upload.fields([
      { name: 'profileImage', maxCount: 1 },
      { name: 'design', maxCount: 1 }
    ])],
    require(`${controllerPath}/characters/update`)
  ],
  [
    'delete /characters/:id',
    [auth, hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/characters/delete`)
  ],

  /*
    CHARACTER COMPANIONS
  */

  [
    'get /character_companions',
    [auth, hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/characterCompanions/all`)
  ],
  [
    'get /character_companions/:id',
    require(`${controllerPath}/characterCompanions/get`)
  ],
  [
    'post /character_companions',
    [auth, hasPermissions(['can_edit_characters']), upload.single('image')],
    require(`${controllerPath}/characterCompanions/create`)
  ],
  [
    'patch /character_companions/:id',
    [auth, upload.single('image')],
    require(`${controllerPath}/characterCompanions/update`)
  ],
  [
    'delete /character_companions/:id',
    [auth, hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/characterCompanions/delete`)
  ],

  /*
    COLOR STATUS
  */

  [
    'get /color_statuses',
    [auth],
    require(`${controllerPath}/colorStatus/all`)
  ],
  [
    'post /color_statuses',
    [auth, hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/colorStatus/create`)
  ],
  [
    'patch /color_statuses/:id',
    [auth],
    require(`${controllerPath}/colorStatus/update`)
  ],
  [
    'delete /color_statuses/:id',
    [auth, hasPermissions(['can_edit_characters'])],
    require(`${controllerPath}/colorStatus/delete`)
  ],


  /*
    ITEMS
  */

  [
    'get /items',
    [auth, hasAtLeastOnePermission(['can_edit_items', 'can_edit_characters' ,'can_edit_users'])],
    require(`${controllerPath}/items/all`)
  ],
  [
    'get /items/:id',
    require(`${controllerPath}/items/get`)
  ],
  [
    'post /items',
    [auth, upload.single('icon'), hasPermissions(['can_edit_items'])],
    require(`${controllerPath}/items/create`)
  ],
  [
    'patch /items/:id',
    [auth, upload.single('icon'), hasPermissions(['can_edit_items'])],
    require(`${controllerPath}/items/update`)
  ],
  [
    'delete /items/:id',
    [auth, hasPermissions(['can_edit_items'])],
    require(`${controllerPath}/items/delete`)
  ],
  [
    'get /tags',
    [auth, hasAtLeastOnePermission(['can_edit_items', 'can_edit_characters' ,'can_edit_users'])],
    require(`${controllerPath}/items/tags`)
  ],
  [
    'get /categories',
    [auth, hasAtLeastOnePermission(['can_edit_items', 'can_edit_characters' ,'can_edit_users'])],
    require(`${controllerPath}/items/categories`)
  ],

  /*
    USER ITEMS
  */

  [
    'get /user_items/:id',
    [auth],
    require(`${controllerPath}/userItems/get`)
  ],
  [
    'post /user_items',
    [auth, hasPermissions(['can_edit_items'])],
    require(`${controllerPath}/userItems/create`)
  ],
  [
    'patch /user_items/:id',
    [auth, hasPermissions(['can_edit_items'])],
    require(`${controllerPath}/userItems/update`)
  ],
  [
    'post /user_items/:id/sell',
    [auth],
    require(`${controllerPath}/userItems/sell`)
  ],
  [
    'delete /user_items/:id',
    [auth, hasPermissions(['can_edit_items'])],
    require(`${controllerPath}/userItems/delete`)
  ],

  /*
    USER SECTIONS
  */

  [
    'post /sections',
    [auth, hasPermissions(['can_edit_sections'])],
    require(`${controllerPath}/userSections/create`)
  ],
  [
    'patch /sections/:id',
    [auth, hasPermissions(['can_edit_sections'])],
    require(`${controllerPath}/userSections/update`)
  ],
  [
    'delete /sections/:id',
    [auth, hasPermissions(['can_edit_sections'])],
    require(`${controllerPath}/userSections/delete`)
  ],

  /*
    SHOPS
  */

  [
    'get /shops',
    require(`${controllerPath}/shops/all`)
  ],
  [
    'get /shops/id/:id',
    require(`${controllerPath}/shops/get_by_id`)
  ],
  [
    'get /shops/:slug',
    require(`${controllerPath}/shops/get_by_slug`)
  ],
  [
    'post /shops',
    [auth, upload.single('icon'), hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shops/create`)
  ],
  [
    'patch /shops/:id',
    [auth, upload.single('icon'), hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shops/update`)
  ],
  [
    'delete /shops/:id',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shops/delete`)
  ],

  /*
    SHOP INVENTORY
  */

  [
    'post /shop_items',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shopItems/create`)
  ],
  [
    'patch /shop_items/:id',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shopItems/update`)
  ],
  [
    'delete /shop_items/:id',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shopItems/delete`)
  ],
  [
    'post /shop_items/:id/buy',
    [auth],
    require(`${controllerPath}/shopItems/buy`)
  ],

  /*
    SHOP COMPANION INVENTORY
  */

  [
    'post /shop_companions',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shopCompanions/create`)
  ],
  [
    'patch /shop_companions/:id',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shopCompanions/update`)
  ],
  [
    'delete /shop_companions/:id',
    [auth, hasPermissions(['can_edit_shops'])],
    require(`${controllerPath}/shopCompanions/delete`)
  ],
  [
    'post /shop_companions/:id/buy',
    [auth],
    require(`${controllerPath}/shopCompanions/buy`)
  ],

  /*
    TRADE INS
  */

  [
    'get /trade_ins',
    [auth],
    require(`${controllerPath}/tradeIns/all`)
  ],
  [
    'get /trade_ins/id/:id',
    [auth],
    require(`${controllerPath}/tradeIns/get_by_id`)
  ],
  [
    'get /trade_ins/:slug',
    [auth],
    require(`${controllerPath}/tradeIns/get_by_slug`)
  ],
  [
    'post /trade_ins',
    [auth, hasPermissions(['can_edit_trade_ins'])],
    require(`${controllerPath}/tradeIns/create`)
  ],
  [
    'patch /trade_ins/:id',
    [auth, hasPermissions(['can_edit_trade_ins'])],
    require(`${controllerPath}/tradeIns/update`)
  ],
  [
    'post /trade_ins/:id/init',
    [auth],
    require(`${controllerPath}/tradeIns/tradeIn`)
  ],
  [
    'post /trade_ins/:id/select',
    [auth],
    require(`${controllerPath}/tradeIns/selectReward`)
  ],
  [
    'post /trade_ins/:id/cancel',
    [auth],
    require(`${controllerPath}/tradeIns/cancel`)
  ],
  [
    'delete /trade_ins/:id',
    [auth, hasPermissions(['can_edit_trade_ins'])],
    require(`${controllerPath}/tradeIns/delete`)
  ],

  /*
    TRADE IN CONDITONS
  */

  [
    'post /trade_in_conditions',
    [auth, hasPermissions(['can_edit_trade_ins'])],
    require(`${controllerPath}/tradeIns/conditions/create`)
  ],
  [
    'patch /trade_in_conditions/:id',
    [auth, hasPermissions(['can_edit_trade_ins'])],
    require(`${controllerPath}/tradeIns/conditions/update`)
  ],
  [
    'delete /trade_in_conditions/:id',
    [auth, hasPermissions(['can_edit_trade_ins'])],
    require(`${controllerPath}/tradeIns/conditions/delete`)
  ],

  /*
    RANKS
  */

  [
    'get /ranks',
    [auth, hasAtLeastOnePermission(['can_edit_users', 'admin'])],
    require(`${controllerPath}/ranks/all`)
  ],
  [
    'get /ranks/:id',
    require(`${controllerPath}/ranks/get`)
  ],
  [
    'post /ranks',
    [auth, upload.single('icon'), hasPermissions(['admin'])],
    require(`${controllerPath}/ranks/create`)
  ],
  [
    'patch /ranks/:id',
    [auth, upload.single('icon'), hasPermissions(['admin'])],
    require(`${controllerPath}/ranks/update`)
  ],
  [
    'delete /ranks/:id',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/ranks/delete`)
  ],

  /*
    TRADES
  */

  [
    'get /trades',
    [auth],
    require(`${controllerPath}/trades/all`)
  ],
  [
    'get /trades/:id',
    [auth, hasPermissions(['can_edit_trades'])],
    require(`${controllerPath}/trades/get`)
  ],
  [
    'post /trades',
    [auth],
    require(`${controllerPath}/trades/create`)
  ],
  [
    'patch /trades/:id',
    [auth],
    require(`${controllerPath}/trades/update`)
  ],
  [
    'delete /trades/:id',
    [auth],
    require(`${controllerPath}/trades/delete`)
  ],

  /*
    BOARDS
  */

  [
    'get /boards',
    [auth],
    require(`${controllerPath}/boards/all`)
  ],
  [
    'get /boards/:id',
    [auth],
    require(`${controllerPath}/boards/get`)
  ],
  [
    'post /boards',
    [auth, upload.single('icon'), hasPermissions(['moderate'])],
    require(`${controllerPath}/boards/create`)
  ],
  [
    'patch /boards/:id',
    [auth, upload.single('icon'), hasPermissions(['moderate'])],
    require(`${controllerPath}/boards/update`)
  ],
  [
    'delete /boards/:id',
    [auth, hasPermissions(['moderate'])],
    require(`${controllerPath}/boards/delete`)
  ],

  /*
    THREADS
  */

  [
    'get /threads/:id',
    [auth],
    require(`${controllerPath}/threads/get`)
  ],
  [
    'post /threads',
    [auth],
    require(`${controllerPath}/threads/create`)
  ],
  [
    'patch /threads/:id',
    [auth],
    require(`${controllerPath}/threads/update`)
  ],
  [
    'post /threads/:id/subscribe',
    [auth],
    require(`${controllerPath}/threads/subscribe`)
  ],
  [
    'delete /threads/:id',
    [auth],
    require(`${controllerPath}/threads/delete`)
  ],

  /*
    POSTS
  */

  [
    'post /posts',
    [auth],
    require(`${controllerPath}/posts/create`)
  ],
  [
    'patch /posts/:id',
    [auth],
    require(`${controllerPath}/posts/update`)
  ],
  [
    'delete /posts/:id',
    [auth],
    require(`${controllerPath}/posts/delete`)
  ],

  /*
    MESSAGES
  */

  [
    'get /messages',
    [auth],
    require(`${controllerPath}/messages/all`)
  ],
  [
    'get /messages/info',
    [auth],
    require(`${controllerPath}/messages/info`)
  ],
  [
    'get /messages/:id',
    [auth, hasPermissions(['can_edit_messages'])],
    require(`${controllerPath}/messages/get`)
  ],
  [
    'post /messages',
    [auth],
    require(`${controllerPath}/messages/create`)
  ],
  [
    'patch /messages/:id',
    [auth],
    require(`${controllerPath}/messages/update`)
  ],
  [
    'post /messages/:id/markread',
    [auth],
    require(`${controllerPath}/messages/markRead`)
  ],

  /*
    FILES
  */

  [
    'get /files/:id',
    require(`${controllerPath}/files/get`)
  ],
  [
    'post /files',
    [auth, upload.single('file')],
    require(`${controllerPath}/files/create`)
  ],
  [
    'patch /files/:id',
    [auth, upload.single('file')],
    require(`${controllerPath}/files/update`)
  ],
  [
    'delete /files/:id',
    [auth, hasPermissions(['can_edit_files'])],
    require(`${controllerPath}/files/delete`)
  ],

  /*
    QUICK CATCH EVENT
  */

  [
    'get /quickCatchEvents',
    [auth, hasPermissions(['can_edit_quickcatchevents'])],
    require(`${controllerPath}/quickCatchEvents/all`)
  ],
  [
    'post /quickCatchEvents',
    [auth, upload.single('image'), hasPermissions(['can_edit_quickcatchevents'])],
    require(`${controllerPath}/quickCatchEvents/create`)
  ],
  [
    'post /quickCatchEvents/check',
    [auth],
    require(`${controllerPath}/quickCatchEvents/check`)
  ],
  [
    'post /quickCatchEvents/:id/catch',
    [auth],
    require(`${controllerPath}/quickCatchEvents/catch`)
  ],
  [
    'patch /quickCatchEvents/:id',
    [auth, upload.single('image'), hasPermissions(['can_edit_quickcatchevents'])],
    require(`${controllerPath}/quickCatchEvents/update`)
  ],
  [
    'delete /quickCatchEvents/:id',
    [auth, hasPermissions(['can_edit_quickcatchevents'])],
    require(`${controllerPath}/quickCatchEvents/delete`)
  ],

  /*
    RATES
  */

  [
    'get /rates',
    [auth, hasAtLeastOnePermission(['admin', 'moderate'])],
    require(`${controllerPath}/rates/all`)
  ],

  /*
    GACHA
  */

  [
    'get /gacha',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/gacha/all`)
  ],
  [
    'post /gacha',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/gacha/create`)
  ],
  [
    'post /gacha/play',
    [auth],
    require(`${controllerPath}/gacha/play`)
  ],
  [
    'patch /gacha/:id',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/gacha/update`)
  ],
  [
    'delete /gacha/:id',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/gacha/delete`)
  ],

  /*
    SLOTS
  */

  [
    'post /slots/play',
    [auth],
    require(`${controllerPath}/slots/play`)
  ],

  /*
    LOGS
  */

  [
    'get /logs',
    [auth, hasAtLeastOnePermission(['admin', 'moderate'])],
    require(`${controllerPath}/logs/all`)
  ],
  [
    'get /users/:id/logs',
    [auth],
    require(`${controllerPath}/logs/get`)
  ],
  [
    'post /users/:id/logs',
    [auth, hasAtLeastOnePermission(['admin', 'moderate'])],
    require(`${controllerPath}/logs/create`)
  ],

  /*
    MENU ITEMS
  */

  [
    'get /menu_items',
    require(`${controllerPath}/menuItems/all`)
  ],
  [
    'post /menu_items',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/menuItems/create`)
  ],
  [
    'patch /menu_items/:id',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/menuItems/update`)
  ],
  [
    'delete /menu_items/:id',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/menuItems/delete`)
  ],

  /*
    SETTINGS
  */

  [
    'get /settings/:key',
    require(`${controllerPath}/settings/get`)
  ],
  [
    'get /settings/group/:group',
    [auth],
    require(`${controllerPath}/settings/by_group`)
  ],
  [
    'post /settings',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/settings/create`)
  ],
  [
    'patch /settings/:key',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/settings/update`)
  ],
  [
    'delete /settings/:key',
    [auth, hasPermissions(['admin'])],
    require(`${controllerPath}/settings/delete`)
  ],
]
