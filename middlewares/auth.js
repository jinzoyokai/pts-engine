module.exports = (req, res, next) => {
  if (!req.user || !req.user.id) {
    res.status(403).json({
      errors: ['🐶💢 Not logged in!']
    });
    return;
  }

  next();
}
