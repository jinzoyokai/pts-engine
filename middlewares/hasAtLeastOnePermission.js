const { intersection } = require('lodash');

module.exports = (permissions) => {
  return (req, res, next) => {
    if (!req.user.permissions.some(permission => permissions.indexOf(permission) !== -1)) {
      res.status(403).json({
        errors: ['🐶💢 Not enough permission!']
      });
      return;
    }

    next();
  }
}
