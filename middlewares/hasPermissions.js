const { intersection } = require('lodash');

module.exports = (permissions) => {
  return (req, res, next) => {
    if (!intersection(req.user.permissions, permissions).length) {
      res.status(403).json({
        errors: ['🐶💢 Not authorized!']
      });
      return;
    }

    next();
  }
}
