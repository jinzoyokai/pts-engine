const currencies =   [
  {
    key: 'currency',
    label: 'Pok� Dollar',
    symbol: 'PD'
  },
  {
    key: 'thorn_reputation_points',
    label: 'Thorn Reputation Points',
    symbol: 'TRP'
  },
  {
    key: 'aurora_faction_points',
    label: 'Aurora Reputation Points',
    symbol: 'AFP'
  },
  {
    key: 'eventide_faction_points',
    label: 'Eventide Reputation Points',
    symbol: 'EFP'
  },
  {
    key: 'casino_tickets',
    label: 'Casino Tickets',
    symbol: 'CT'
  },
  {
    key: 'pledge_points',
    label: 'Pledge Points',
    symbol: 'PP'
  },
]
module.exports = {
  currencies,
  currencyKeys: currencies.map(({key}) => key)
};
