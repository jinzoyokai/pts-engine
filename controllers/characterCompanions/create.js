const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');
const { getOtherSection } = require('@/mixins/users.js');

const CharacterCompanion = require('@/models/characterCompanion.js');

module.exports = async (req, res) => {
  let characterCompanion = new CharacterCompanion();

  characterCompanion.colorStatuses = req.body.colorStatuses;
  characterCompanion.design = req.body.design;
  characterCompanion.level = req.body.level;
  characterCompanion.pokeball = req.body.pokeball;
  characterCompanion.notes = req.body.notes ? sanitize(req.body.notes) : undefined;
  characterCompanion.mod_notes = req.body.mod_notes ? sanitize(req.body.mod_notes) : undefined;
  characterCompanion.team = req.body.team;
  characterCompanion.salon = req.body.salon;
  characterCompanion.companion = req.body.companion;
  characterCompanion.user = req.body.user;
  characterCompanion.character = req.body.character;

  if(!characterCompanion.character) {
    characterCompanion.section = await getOtherSection(req.body.user);
  }

  if(!!req.file && !!req.file.location)
    characterCompanion.image = req.file.location;

  try {
    await log(characterCompanion, 'companion', 'save', req.user);
    res.json(await characterCompanion.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
