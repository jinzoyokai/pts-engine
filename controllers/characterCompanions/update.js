const { pick, omit } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js'),
      Character = require('@/models/character.js'),
      Section = require('@/models/userSection.js'),
      CharacterCompanion = require('@/models/characterCompanion.js');

const UserPolicy = require('@/policies/user.js'),
      CharacterPolicy = require('@/policies/character.js'),
      CharacterCompanionPolicy = require('@/policies/characterCompanion.js');

module.exports = async (req, res) => {
  try {
    const characterCompanion = await CharacterCompanion.findById(req.params.id);

    const characterCompanionPolicy = new CharacterCompanionPolicy(req.user, characterCompanion);

    if(!characterCompanionPolicy.canWrite())
      throw { name: 'PermissionError' };

    let payload = {
      ...omit(req.body, ['user', 'character', 'section'])
    };

    if(!!req.body.user) {
      const user = await User.findById(req.body.user);
      const userPolicy = new UserPolicy(req.user, user);

      if(!userPolicy.canWrite())
        throw { name: 'PermissionError' };

      payload.user = user._id;
    }


    if(req.body.character !== undefined) {
      if(!!req.body.character) {
        const character = await Character.findById(req.body.character);
        const characterPolicy = new CharacterPolicy(req.user, character);

        if(!characterPolicy.canWrite())
          throw { name: 'PermissionError' };

        payload.character = character._id;
      } else {
        payload.character = null;
      }
    }

    if(req.body.section !== undefined) {
      if(!!req.body.section) {
        const section = await Section.findById(req.body.section);

        if(!section)
          throw { name: 'NotFoundError' };

        payload.section = section._id;
      } else {
        payload.section = null;
      }
    }

    if(!!req.file && !!req.file.location) {
      payload.image = req.file.location;

      if(characterCompanion.image)
        fs.unlink(characterCompanion.image, () => {
          // file deleted
        });
    }

    if(!payload.colorStatuses)
      payload.colorStatuses = null;

    console.log(characterCompanionPolicy.write())
    payload = pick(payload, characterCompanionPolicy.write());

    await CharacterCompanion.updateOne({ _id: req.params.id }, payload);
    res.json(await CharacterCompanion.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
