const { handleValidationError } = require('@/mixins/validation.js');

const CharacterCompanion = require('@/models/characterCompanion.js');

const CharacterCompanionPolicy = require('@/policies/characterCompanion.js');

module.exports = async (req, res) => {
  const characterCompanionPolicy = new CharacterCompanionPolicy(req.user);

  try {
    res.json(
      await CharacterCompanion.find({})
                              .select(characterCompanionPolicy.read())
                              .populate('companion')
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
