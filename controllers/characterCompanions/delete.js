const { handleValidationError } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');

const CharacterCompanion = require('@/models/characterCompanion.js')

module.exports = async (req, res) => {
  try {
    const characterCompanion = await CharacterCompanion.findById(req.params.id);

    if(!characterCompanion)
      throw { name: 'NotFoundError' };

    await log(characterCompanion, 'companion', 'remove', req.user);
    res.json(await characterCompanion.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
