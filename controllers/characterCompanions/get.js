const { pick } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const CharacterCompanion = require('@/models/characterCompanion.js');

const CharacterCompanionPolicy = require('@/policies/characterCompanion.js');

module.exports = async (req, res) => {
  try {
    const characterCompanion = await CharacterCompanion
      .findById(req.params.id)
      .populate('companion');

    if(!characterCompanion)
      throw { name: 'NotFoundError' };

    const characterCompanionPolicy = new CharacterCompanionPolicy(req.user, characterCompanion);

    res.json(
      pick(characterCompanion, characterCompanionPolicy.read())
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
