const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Submission = require('@/models/submission.js')

module.exports = async (req, res) => {
  let submission = new Submission();

  submission.user = req.user;
  submission.description = sanitize(req.body.description);

  if(!!req.file && !!req.file.location) {
    submission.image = req.file.location;
  } else {
    return res.status(422).json({
      errors: ['Please provide a valid image.']
    });
  }

  try {
    let savedSubmission = await submission.save()
    await savedSubmission.generateThumbnail();
    res.json(savedSubmission)
  } catch (error) {
    return handleValidationError(error, res)
  }
}
