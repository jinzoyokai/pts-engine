const { handleValidationError } = require('@/mixins/validation.js');

const Submission = require('@/models/submission.js')

const SubmissionPolicy = require('@/policies/submission.js');

module.exports = async (req, res) => {
  try {
    const submission = await Submission.findById(req.params.id);

    if(!submission)
      throw { name: 'NotFoundError' };

    const submissionPolicy = new SubmissionPolicy(req.user, submission);

    if(!submissionPolicy.canWrite())
      throw { name: 'PermissionError' };

    res.json(await submission.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
