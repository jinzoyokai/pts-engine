const { handleValidationError } = require('@/mixins/validation.js');

const Submission = require('@/models/submission.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Submission.find({}).populate({
        path: 'user',
        select: ['_id', 'username']
      })
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
