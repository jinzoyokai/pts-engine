const { handleValidationError } = require('@/mixins/validation.js');

const Companion = require('@/models/companion.js');

module.exports = async (req, res) => {

  try {
    const companion = await Companion.findById(req.params.id);

    if(!companion)
      throw { name: 'NotFoundError' };

    res.json(companion);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
