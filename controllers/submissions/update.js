const { pick } = require('lodash'),
      fs = require('fs'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Submission = require('@/models/submission.js')

const SubmissionPolicy = require('@/policies/submission.js');

module.exports = async (req, res) => {
  try {
    const submission = await Submission.findById(req.params.id);

    const submissionPolicy = new SubmissionPolicy(req.user, submission);

    if(!submissionPolicy.canWrite())
      throw { name: 'PermissionError' };

    let payload = req.body;

    payload = pick(payload, submissionPolicy.write());

    if(payload.description) {
      payload.description = sanitize(payload.description);
    }

    await Submission.updateOne({ _id: req.params.id }, payload)
    res.json(await Submission.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
