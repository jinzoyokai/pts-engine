const { handleValidationError } = require('@/mixins/validation.js');

const Submission = require('@/models/submission.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Submission.paginate({
        'hidden' : false,
        'status' : 'approved'
      }, {
        populate: [{
          path: 'user',
          select: ['_id', 'username']
        }],
        sort: '-createdAt',
        page: req.query.page || 1,
        limit: 12
      })
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
