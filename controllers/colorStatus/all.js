const { handleValidationError } = require('@/mixins/validation.js');

const ColorStatus = require('@/models/colorStatus.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await ColorStatus.find({})
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
