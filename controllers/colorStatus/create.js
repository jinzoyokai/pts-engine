const { handleValidationError } = require('@/mixins/validation.js');

const ColorStatus = require('@/models/colorStatus.js');

module.exports = async (req, res) => {
  let colorStatus = new ColorStatus();

  colorStatus.name = req.body.name;
  colorStatus.css = req.body.css;

  try {
    res.json(await colorStatus.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
