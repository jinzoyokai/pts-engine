const { handleValidationError } = require('@/mixins/validation.js');

const ColorStatus = require('@/models/colorStatus.js');

module.exports = async (req, res) => {
  try {
    const colorStatus = await ColorStatus.findById(req.params.id);
    const payload = req.body;

    await ColorStatus.updateOne({ _id: req.params.id }, payload);
    res.json(await ColorStatus.findById(req.params.id))
  } catch (error) {
    console.log(error)
    return handleValidationError(error, res);
  }
}
