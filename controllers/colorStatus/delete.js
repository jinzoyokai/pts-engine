const { handleValidationError } = require('@/mixins/validation.js');

const ColorStatus = require('@/models/colorStatus.js')

module.exports = async (req, res) => {
  try {
    const colorStatus = await ColorStatus.findById(req.params.id);

    if(!colorStatus)
      throw { name: 'NotFoundError' };

    res.json(await colorStatus.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
