const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');
const { sendSystemMessage } = require('@/mixins/messaging.js');

const Trade = require('@/models/trade.js')

const TradePolicy = require('@/policies/trade.js');

module.exports = async (req, res) => {
  try {
    const trade = await Trade.findById(req.params.id)
                             .populate('from')
                             .populate('to')
                             .populate('items')
                             .populate('companions');

    if(!trade)
      throw { name: 'NotFoundError' };

    const tradePolicy = new TradePolicy(req.user, trade);

    if(!tradePolicy.canDelete())
      throw { name: 'PermissionError' };

    if(!!trade.fromCurrency) {
      trade.from.currency += parseInt(trade.fromCurrency);
      await trade.from.save();
    }

    if(!!trade.accepted && !!trade.toCurrency) {
      trade.to.currency += parseInt(trade.toCurrency);
      await trade.to.save();
    }

    if(
      !!trade.items
      && trade.items.length
    )
      for(const item of trade.items) {
        item.trade = null;
        await item.save();
      }

    if(
      !!trade.companions
      && trade.companions.length
    )
      for(const companion of trade.companions) {
        companion.trade = null;
        await companion.save();
      }

    await log(trade, 'trade', 'remove', req.user);

    if(!!req.body.canceled) {
      const reason = req.body.reason ? `Reason: ${sanitize(req.body.reason)}` : 'No reason given.';

      await sendSystemMessage(`Your trade with "${trade.from.username}" was canceled.`, reason, trade.to._id);
      await sendSystemMessage(`Your trade with "${trade.to.username}" was canceled.`, reason, trade.from._id);
    }

    await trade.remove()

    res.json(true);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
