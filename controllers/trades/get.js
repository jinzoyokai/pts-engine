const { handleValidationError } = require('@/mixins/validation.js');

const Trade = require('@/models/trade.js');

module.exports = async (req, res) => {

  try {
    const trade = await Trade.findById(req.params.id);

    if(!trade)
      throw { name: 'NotFoundError' };

    res.json(trade);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
