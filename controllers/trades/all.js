const { handleValidationError } = require('@/mixins/validation.js');

const Trade = require('@/models/trade.js')

module.exports = async (req, res) => {
  try {
    let result;

    if(req.user.permissions.indexOf('can_edit_trades') !== -1) {
      result = await Trade.find({})
      .sort('-createdAt')
      .populate({
        path: 'fromItems',
        populate: {
          path: 'item'
        },
      })
      .populate({
        path: 'toItems',
        populate: {
          path: 'item'
        },
      })
      .populate({
        path: 'fromCompanions',
        populate: {
          path: 'companion'
        },
      })
      .populate({
        path: 'toCompanions',
        populate: {
          path: 'companion'
        },
      })
      .populate({
        path: 'from',
        select: ['_id', 'username']
      })
      .populate({
        path: 'to',
        select: ['_id', 'username']
      });
    } else {
      result = await Trade.find({
        $or: [
          { from: req.user._id },
          { to: req.user._id }
        ]
      })
      .sort('-createdAt')
      .populate({
        path: 'fromItems',
        populate: {
          path: 'item'
        },
      })
      .populate({
        path: 'toItems',
        populate: {
          path: 'item'
        },
      })
      .populate({
        path: 'fromCompanions',
        populate: {
          path: 'companion'
        },
      })
      .populate({
        path: 'toCompanions',
        populate: {
          path: 'companion'
        },
      })
      .populate({
        path: 'from',
        select: ['_id', 'username']
      })
      .populate({
        path: 'to',
        select: ['_id', 'username']
      });
    }

    res.json(result);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
