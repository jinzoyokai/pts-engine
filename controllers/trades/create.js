const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');

const Trade = require('@/models/trade.js');
const User = require('@/models/user.js');
const UserItem = require('@/models/userItem.js');
const CharacterCompanion = require('@/models/characterCompanion.js');

const UserItemPolicy = require('@/policies/userItem.js');
const CharacterCompanionPolicy = require('@/policies/characterCompanion.js');

module.exports = async (req, res) => {
  let trade = new Trade();

  trade.from = req.user._id;
  trade.comment = req.body.comment;

  let to = req.body.to;

  if(
    !to
    || to === req.user_id
  )
    return res.status(422).json({
      errors: ['"to" needs to be a valid user id.']
    });


  try {
      const toUser = await User.findById(to);

      if(!toUser)
        throw { name: 'NotFoundError' };

      trade.to = toUser;

      /* ITEMS */

      if(
        !!req.body.fromItems &&
        req.body.fromItems.length > 0
      )
        for(const item of req.body.fromItems) {
          const userItem = await UserItem.findById(item);

          if(!userItem || !!userItem.trade)
            return res.status(422).json({
              errors: ['One or more items are not available for trading.']
            });

          const userItemPolicy = new UserItemPolicy(req.user, userItem);

          if(!userItemPolicy.canWrite())
            throw { name: 'PermissionError' };
        }

      trade.fromItems = req.body.fromItems;

      if(
        !!req.body.toItems &&
        req.body.toItems.length > 0
      )
        for(const item of req.body.toItems) {
          const userItem = await UserItem.findById(item);

          if(!userItem || !!userItem.trade)
            return res.status(422).json({
              errors: ['One or more items are not available for trading.']
            });
        }

      trade.toItems = req.body.toItems;

      /* COMPANIONS */

      if(
        !!req.body.fromCompanions &&
        req.body.fromCompanions.length > 0
      )
        for(const item of req.body.fromCompanions) {
          const characterCompanion = await CharacterCompanion.findById(item);

          if(!characterCompanion || !!characterCompanion.trade)
            return res.status(422).json({
              errors: ['One or more pokemon are not available for trading.']
            });

          const characterCompanionPolicy = new CharacterCompanionPolicy(req.user, characterCompanion);

          if(!characterCompanionPolicy.canWrite())
            throw { name: 'PermissionError' };
        }

      trade.fromCompanions = req.body.fromCompanions;

      if(
        !!req.body.toCompanions &&
        req.body.toCompanions.length > 0
      )
        for(const item of req.body.toCompanions) {
          const characterCompanion = await CharacterCompanion.findById(item);

          if(!characterCompanion || !!characterCompanion.trade)
            return res.status(422).json({
              errors: ['One or more pokemon are not available for trading.']
            });
        }

      trade.toCompanions = req.body.toCompanions;

      const toCurrency = parseInt(req.body.toCurrency);

      if(!!toCurrency && toCurrency > 0)
        trade.toCurrency = toCurrency;

      const fromCurrency = parseInt(req.body.fromCurrency);
      
      if(!!fromCurrency && fromCurrency > 0) {
        if(fromCurrency > req.user.currency)
          return res.status(422).json({
            errors: ['You don\'t have enough currency to create this trade']
          });

        trade.fromCurrency = fromCurrency;
      }


    await trade.save();

    if(
      !!trade.fromItems &&
      trade.fromItems.length > 0
    )
      for(const item of trade.fromItems) {
        const userItem = await UserItem.findById(item);

        userItem.trade = trade;
        userItem.save();
      }

    if(
      !!trade.fromCompanions &&
      trade.fromCompanions.length > 0
    )
      for(const companion of trade.fromCompanions) {
        const characterCompanion = await CharacterCompanion.findById(companion);

        characterCompanion.trade = trade;
        characterCompanion.save();
      }

    if(!!trade.fromCurrency) {
      req.user.currency -= parseInt(trade.fromCurrency);

      await req.user.save();
    }

    await log(trade, 'trade', 'save', req.user);
    res.json(trade)
  } catch (error) {
    return handleValidationError(error, res)
  }
}
