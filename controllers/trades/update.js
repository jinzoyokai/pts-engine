const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js'),
      { getOtherSection } = require('@/mixins/users.js'),
      { log } = require('@/mixins/logging.js');

const Trade = require('@/models/trade.js');
const UserItem = require('@/models/userItem.js');
const CharacterCompanion = require('@/models/characterCompanion.js');

const TradePolicy = require('@/policies/trade.js');
const UserItemPolicy = require('@/policies/userItem.js');
const CharacterCompanionPolicy = require('@/policies/userItem.js');

module.exports = async (req, res) => {
  try {
    const trade = await Trade.findById(req.params.id)
                             .populate('from')
                             .populate('fromItems')
                             .populate('fromCompanions')
                             .populate('to')
                             .populate('toItems')
                             .populate('toCompanions');

    const tradePolicy = new TradePolicy(req.user, trade);

    if(!tradePolicy.canWrite())
      throw { name: 'PermissionError' };

    const payload = req.body;

    const writeableFields = tradePolicy.write();

    let newBalance;

    if(
      !trade.accepted
      && !!payload.accepted
      && writeableFields.indexOf('accepted') !== -1
    ) {
      await log(trade, 'trade', 'accept', req.user);
      trade.to.currency -= trade.toCurrency;
      await trade.to.save();

      newBalance = trade.to.currency;

      if(
        !!trade.toItems
        && trade.toItems.length
      ) {
        for(const userItem of trade.toItems) {
          if(!userItem || !!userItem.trade)
            return res.status(422).json({
              errors: ['One or more items are not available for trading.']
            });
        }

        for(const userItem of trade.toItems) {
          userItem.trade = trade;
          userItem.save();
        }
      }

      if(
        !!trade.toCompanions
        && trade.toCompanions.length
      ) {
        for(const characterCompanion of trade.toCompanions) {
          if(!characterCompanion || !!characterCompanion.trade)
            return res.status(422).json({
              errors: ['One or more pokemon are not available for trading.']
            });
        }

        for(const characterCompanion of trade.toCompanions) {
          characterCompanion.trade = trade;
          characterCompanion.save();
        }
      }
    }

    if(
      !trade.approved
      && !!payload.approved
      && writeableFields.indexOf('approved') !== -1
    ) {
      await log(trade, 'trade', 'approve', req.user);
      if(!!trade.fromCurrency)
        trade.to.currency += trade.fromCurrency;

      if(!!trade.toCurrency)
        trade.from.currency += trade.toCurrency;

      if(
        !!trade.fromItems
        && trade.fromItems.length
      )
        for(const userItem of trade.fromItems) {
          userItem.user = trade.to;
          userItem.trade = null;
          userItem.save();
        }

      if(
        !!trade.toItems
        && trade.toItems.length
      )
        for(const userItem of trade.toItems) {
          userItem.user = trade.from;
          userItem.trade = null;
          userItem.save();
        }

      if(
        !!trade.fromCompanions
        && trade.fromCompanions.length
      ) {
        const otherSection = await getOtherSection(trade.to);

        for(const characterCompanion of trade.fromCompanions) {
          characterCompanion.user = trade.to;
          characterCompanion.character = null;
          characterCompanion.section = otherSection;
          characterCompanion.trade = null;
          characterCompanion.save();
        }
      }

      if(
        !!trade.toCompanions
        && trade.toCompanions.length
      ) {
        const otherSection = await getOtherSection(trade.from);

        for(const characterCompanion of trade.toCompanions) {
          characterCompanion.user = trade.from;
          characterCompanion.character = null;
          characterCompanion.section = otherSection;
          characterCompanion.trade = null;
          characterCompanion.save();
        }
      }

      await trade.to.save();
      await trade.from.save();
    }

    await Trade.updateOne(
      { _id: req.params.id },
      pick(payload, writeableFields)
    );
    res.json({
      trade: await Trade.findById(req.params.id),
      newBalance
    })
  } catch (error) {
    return handleValidationError(error, res);
  }
}
