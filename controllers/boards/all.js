const { handleValidationError } = require('@/mixins/validation.js');

const Board = require('@/models/board.js');
const Post = require('@/models/post.js');

module.exports = async (req, res) => {
  try {
    let boards = await Board.find({}).populate({
      path: 'posts',
      populate: [
        {
          path: 'user',
          select: '_id username',
        },
        'thread'
      ],
      options: {
        perDocumentLimit: 1,
        sort: { createdAt: -1 },
      }
    });

    for (let i = 0; i < boards.length; i++) {
      const newestPost = await Post.findOne({board: boards[i]})
                                   .populate('thread')
                                   .populate('user');

      boards[i].newestPost = newestPost;
    }

    res.json(boards);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
