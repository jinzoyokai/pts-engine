const { handleValidationError } = require('@/mixins/validation.js');

const Board = require('@/models/board.js')

module.exports = async (req, res) => {
  try {
    const board = await Board.findById(req.params.id);

    if(!board)
      throw { name: 'NotFoundError' };

    res.json(await board.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
