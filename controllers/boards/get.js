const { handleValidationError } = require('@/mixins/validation.js');

const Board = require('@/models/board.js');
const Thread = require('@/models/thread.js');
const Post = require('@/models/post.js');

module.exports = async (req, res) => {
  try {
    const board = await Board.findById(req.params.id)

    const options = {
      path: 'posts',
      populate: [
        {
          path: 'user',
          select: '_id username',
        }
      ],
      options: {
        perDocumentLimit: 1,
        sort: { createdAt: -1 },
      }
    };

    const [threads, stickiedThreads] = await Promise.all([
      Thread.paginate({
        board,
        sticky: false
      }, {
        page: req.query.page || 1,
        populate: [options]}
      ),
      Thread.find({
        board,
        sticky: true
      }).populate(options)
    ]);

    res.json({
      board,
      threads,
      stickiedThreads
    });
  } catch (error) {
    return handleValidationError(error, res)
  }
}
