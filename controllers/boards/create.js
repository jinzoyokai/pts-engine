const { handleValidationError } = require('@/mixins/validation.js');

const Board = require('@/models/board.js');

module.exports = async (req, res) => {
  const board = new Board();

  board.name = req.body.name;
  board.description = req.body.description;
  board.locked = req.body.locked;
  board.order = req.body.order;

  if(!!req.file && !!req.file.location)
    board.icon = req.file.location;

  try {
    res.json(await board.save());
  } catch (error) {
    return handleValidationError(error, res);
  }
}
