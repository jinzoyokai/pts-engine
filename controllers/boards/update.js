const { handleValidationError } = require('@/mixins/validation.js');

const Board = require('@/models/board.js')

module.exports = async (req, res) => {
  try {
    await Board.findById(req.params.id);

    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;
    } else {
      delete payload.icon;
    }

    await Board.updateOne({ _id: req.params.id }, payload)
    res.json(await Board.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
