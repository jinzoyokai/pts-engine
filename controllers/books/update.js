const { handleValidationError } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Book = require('@/models/book.js')

module.exports = async (req, res) => {
  try {
    const book = await Book.findById(req.params.id);

    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;
    } else {
      delete payload.icon;
    }

    await Book.updateOne({ _id: req.params.id }, payload)
    res.json(await Book.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
