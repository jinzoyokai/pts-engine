const { handleValidationError } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Book = require('@/models/book.js')

module.exports = async (req, res) => {
  let book = new Book();

  book.name = req.body.name;
  book.value = req.body.value;
  book.slug = req.body.slug;

  if(!!req.file && !!req.file.location)
    book.icon = req.file.location;

  try {
    res.json(await book.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
