const { handleValidationError } = require('@/mixins/validation.js');

const Book = require('@/models/book.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Book.find({})
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
