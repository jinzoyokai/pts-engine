const { handleValidationError } = require('@/mixins/validation.js');

const Book = require('@/models/book.js')

module.exports = async (req, res) => {
  try {
    const book = await Book.findById(req.params.id);

    if(!book)
      throw { name: 'NotFoundError' };

    res.json(await book.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
