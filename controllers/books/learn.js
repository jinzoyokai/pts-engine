const { sum } = require('lodash');

const { handleValidationError } = require('@/mixins/validation.js');

const Book = require('@/models/book.js'),
      Rank = require('@/models/rank.js'),
      Character = require('@/models/character.js'),
      CharacterBook = require('@/models/characterBook.js');

module.exports = async (req, res) => {
  try {
    const book = Book.findOne({ slug : req.params.slug });

    if(!book)
      throw { name: 'NotFoundError' };

    const requests = [];

    const characters = req.user.characters;

    characters.forEach((character) => {
      requests.push(
        new Promise(async(resolve, reject) => {
          let characterBook = await CharacterBook.findOne({
            character: character._id,
            book: book._id
          });

          if(!characterBook) {
            characterBook = new CharacterBook();

            characterBook.character = character._id;
            characterBook.book = book._id;
            await characterBook.save();

            resolve(true);
          } else {
            resolve(false);
          }
        })
      );
    });

    let characterBookResults = await Promise.all(requests);

    res.json({
      book,
      result: characters.length === characterBookResults.filter(r => !r).length ? 'already_learned' : 'learned'
    });
  } catch (error) {
    return handleValidationError(error, res);
  }
}
