const { handleValidationError } = require('@/mixins/validation.js');

const Companion = require('@/models/companion.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Companion.find({})
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
