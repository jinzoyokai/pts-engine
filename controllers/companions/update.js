const fs = require('fs'),
      { handleValidationError } = require('@/mixins/validation.js');

const Companion = require('@/models/companion.js')

module.exports = async (req, res) => {
  try {
    const companion = await Companion.findById(req.params.id);

    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;

      if(companion.icon)
        fs.unlink(companion.icon, () => {
          // file deleted
        });
    }

    await Companion.updateOne({ _id: req.params.id }, payload)
    res.json(await Companion.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
