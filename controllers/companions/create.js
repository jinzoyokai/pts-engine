const { handleValidationError } = require('@/mixins/validation.js');

const Companion = require('@/models/companion.js')

module.exports = async (req, res) => {
  let companion = new Companion();

  companion.name = req.body.name;

  if(!!req.file && !!req.file.location)
    companion.icon = req.file.location;

  try {
    res.json(await companion.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
