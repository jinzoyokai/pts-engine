const { handleValidationError } = require('@/mixins/validation.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');

module.exports = async (req, res) => {
  let tradeIn = new TradeIn();

  const existingTradeIn = await TradeIn.findOne({ slug: req.body.slug });

  if(existingTradeIn)
    return res.status(422).json({
      errors: ['There\'s already content under that 🐌 slug.']
    });

  tradeIn.name = req.body.name;
  tradeIn.description = req.body.description;
  tradeIn.slug = req.body.slug;
  tradeIn.price = req.body.price;
  tradeIn.currency = req.body.currency;
  tradeIn.numberOfItemsRequired = req.body.numberOfItemsRequired;
  tradeIn.showChoices = req.body.showChoices;
  tradeIn.numberOfChoices = req.body.numberOfChoices;
  tradeIn.isCancelable = req.body.isCancelable;
  tradeIn.isLimited = req.body.isLimited;
  tradeIn.limitValue = req.body.limitValue;
  tradeIn.limitType = req.body.limitType;

  try {
    res.json(await tradeIn.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
