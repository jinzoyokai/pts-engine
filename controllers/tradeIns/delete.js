const { handleValidationError } = require('@/mixins/validation.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js')

module.exports = async (req, res) => {
  try {
    const tradeIn = await TradeIn.findById(req.params.id);

    if(!tradeIn)
      throw { name: 'NotFoundError' };

    res.json(await tradeIn.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
