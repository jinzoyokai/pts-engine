const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const TradeInCondition = require('@/models/tradeIns/condition.js'),
      TradeIn = require('@/models/tradeIns/tradeIn.js');

module.exports = async (req, res) => {
  let tradeIn,
      parent,
      tradeInCondition = new TradeInCondition();

  try {
    tradeIn = await TradeIn.findById(req.body.tradeIn);
  } catch (error) {
    return handleValidationError(error, res)
  }

  tradeInCondition.tradeIn = tradeIn;
  tradeInCondition.items = req.body.items;
  tradeInCondition.rewards = req.body.rewards;

  try {
    res.json(await tradeInCondition.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
