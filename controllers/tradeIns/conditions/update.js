const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const TradeInCondition = require('@/models/tradeIns/condition.js');

module.exports = async (req, res) => {
  try {
    const tradeInCondition = await TradeInCondition.findById(req.params.id);

    const updatedTradeInCondition = req.body;

    await TradeInCondition.updateOne({ _id: req.params.id }, updatedTradeInCondition)
    res.json(await TradeInCondition.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
