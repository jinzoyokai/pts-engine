const { handleValidationError } = require('@/mixins/validation.js');

const TradeInCondition = require('@/models/tradeIns/condition.js');

module.exports = async (req, res) => {
  try {
    const tradeInCondition = await TradeInCondition.findById(req.params.id);

    if(!tradeInCondition)
      throw { name: 'NotFoundError' };

    res.json(await tradeInCondition.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
