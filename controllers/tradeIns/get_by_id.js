const { handleValidationError } = require('@/mixins/validation.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');


module.exports = async (req, res) => {
  try {
    const tradeIn = await TradeIn.findById(req.params.id)
    .populate({
      path: 'conditions',
      populate : {
        path : 'rewards',
      },
    });

    if(!tradeIn)
      throw { name: 'NotFoundError' };

    res.json(tradeIn);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
