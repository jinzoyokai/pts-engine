const { handleValidationError } = require('@/mixins/validation.js');
const { handleRewardSet, checkTradeInLimit, finishTradeIn } = require('@/mixins/tradeIns.js');
const { isEqual } = require('lodash');
const { log } = require('@/mixins/logging.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');
const Pool = require('@/models/tradeIns/pool.js');
const UserItem = require('@/models/userItem.js');
const User = require('@/models/user.js');

const UserItemPolicy = require('@/policies/userItem.js');

module.exports = async (req, res) => {
  try {
    /* SETUP */
    const tradeIn = await TradeIn.findOne({ _id : req.params.id })
    .populate({
      path: 'conditions',
      populate : {
        path : 'items rewards',
        populate : {
          path : 'companions items',
        },
      },
    });

    if(!tradeIn)
      throw { name: 'NotFoundError' };

    if(tradeIn.isLimited) {
      if(await checkTradeInLimit(tradeIn, req.user))
        throw { name: 'PermissionError' };
    }

    const existingPool = await Pool.findOne({
      tradeIn,
      user: req.user
    })

    if(existingPool)
      throw { name: 'PermissionError' };



    /* VALIDATE ITEMS & GET CONDITION */
    if(
      !req.body.items
      || req.body.items.length < tradeIn.numberOfItemsRequired
    )
      throw { name: 'NotFoundError' };

    const userItems = [];

    for(let item of req.body.items) {
      let userItem = await UserItem.findOne({ _id : item._id }).populate('item')

      if(!userItem)
        throw { name: 'NotFoundError' };

      let userItemPolicy = new UserItemPolicy(req.user, userItem);

      if(!userItemPolicy.canWrite())
        throw { name: 'PermissionError' };

      userItems.push(userItem);
    }

    const userItemsIds = userItems.map(
      ({item}) => String(item._id)
    ).sort();

    let matchedCondition;

    for(let condition of tradeIn.conditions) {
      const items = condition.items.map(
        ({_id}) => String(_id)
      ).sort();
      if(isEqual(userItemsIds.sort(), items.sort())) {
        matchedCondition = condition;
        break;
      }
    }

    if(!matchedCondition)
      return res.status(422).json({
        errors: ['No matching condition found.']
      });

    /* PAYMENT */
    let newBalance;
    if(
      tradeIn.price > 0
    ) {
      if(
        req.user[tradeIn.currency] < tradeIn.price
      )
        return res.status(422).json({
          errors: ['You don\'t have sufficient funds.']
        });

      const updatedUser = {};

      newBalance = req.user[tradeIn.currency] - tradeIn.price;

      updatedUser[tradeIn.currency] = newBalance;
      req.user[tradeIn.currency] = newBalance;
      const test = await User.updateOne({ _id: req.user._id }, updatedUser);
    }

    /* GENERATE REWARDS */
    const draw = () => {
        let weighted_rewards = [];

        for(reward of matchedCondition.rewards) {
          let weight = reward.weight == 0 ? 1 : reward.weight;

          for(let i=0; i<weight; i++) {
              weighted_rewards.push(reward);
          }
        }

        return weighted_rewards[Math.floor(Math.random() * weighted_rewards.length)] || matchedCondition.rewards[0];
    };

    /* IF CHOICES: GENERATE POOL */
    if(tradeIn.showChoices) {
      const pool = new Pool();

      pool.tradeIn = tradeIn;
      pool.user = req.user;
      pool.items = userItems;
      pool.rewards = [];

      await Promise.all(userItems.map((userItem) => {
        userItem.user = null;

        return userItem.save();
      }));

      for(let i = 0; i < tradeIn.numberOfChoices; i++) {
        let j = 0;
        let rewardSet;

        let strRewards = pool.rewards.map((reward) => JSON.stringify(reward))

        while (j < 100) {
          j++;
          rewardSet = draw();

          if(strRewards.indexOf(JSON.stringify(rewardSet)) === -1) {
            break;
          }
        }


        pool.rewards.push(rewardSet);
      }

      pool.save();
      await finishTradeIn(tradeIn, req.user)

      await log({
        tradeIn,
        userItems,
        user: req.user
      }, 'tradeIn', 'tradeInStart', req.user);

      return res.json({
        pool,
        newBalance
      })
    /* ELSE: GIVE REWARD */
    } else {
      const rewardSet = draw();

      const rewards = await handleRewardSet(rewardSet, req.user, userItems)
      await finishTradeIn(tradeIn, req.user)

      await log({
        tradeIn,
        rewards,
        userItems,
        user: req.user
      }, 'tradeIn', 'tradeIn', req.user);

      return res.json({
        rewards,
        newBalance
      })
    }
  } catch (error) {
    return handleValidationError(error, res)
  }
}
