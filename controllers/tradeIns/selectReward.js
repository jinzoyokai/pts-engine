const { handleValidationError } = require('@/mixins/validation.js');
const { handleRewardSet } = require('@/mixins/tradeIns.js');
const { log } = require('@/mixins/logging.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');
const Pool = require('@/models/tradeIns/pool.js');

module.exports = async (req, res) => {
  try {
    /* SETUP */
    const tradeIn = await TradeIn.findOne({ _id : req.params.id })
    .populate({
      path: 'conditions',
      populate : {
        path : 'items rewards',
        populate : {
          path : 'companion item',
        },
      },
    });

    if(!tradeIn)
      throw { name: 'NotFoundError' };

    const pool = await Pool.findOne({
      tradeIn,
      user: req.user
    })
    .populate({
      path: 'items rewards.items rewards.companions',
    })


    if(!pool)
      throw { name: 'NotFoundError' };

    if(
      !req.body.select == undefined
      || isNaN(req.body.select)
    )
      return res.status(422).json({
        errors: ['Please select a reward.']
      });

    const rewardSet = pool.rewards[req.body.select]

    if(!rewardSet)
      return res.status(422).json({
        errors: ['Please select a valid reward.']
      });

    const rewards = await handleRewardSet(rewardSet, req.user, pool.items);

    await log({
      tradeIn,
      rewards,
      user: req.user
    }, 'tradeIn', 'tradeInEnd', req.user);

    await pool.remove();

    return res.json({
      rewards
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
