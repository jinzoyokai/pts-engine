const { flatten } = require('lodash');

const { handleValidationError } = require('@/mixins/validation.js');
const { checkTradeInLimit } = require('@/mixins/tradeIns.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');
const Pool = require('@/models/tradeIns/pool.js');


module.exports = async (req, res) => {
  try {
    const tradeIn = await TradeIn.findOne({ slug : req.params.slug })
    .populate({
      path: 'conditions',
    });

    if(!tradeIn)
      throw { name: 'NotFoundError' };

    let result = tradeIn.toJSON();

    result.conditions = flatten(result.conditions.map(({items}) => {
      return items;
    }))

    const pool = await Pool.findOne({
      tradeIn,
      user: req.user
    })
    .populate({
      path: 'rewards.items rewards.companions',
    });

    let limitReached = false;
    if(tradeIn.isLimited)
      limitReached = await checkTradeInLimit(tradeIn, req.user)

    res.json({
      tradeIn: result,
      pool,
      limitReached
    });
  } catch (error) {
    return handleValidationError(error, res)
  }
}
