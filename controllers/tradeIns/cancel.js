const { handleValidationError } = require('@/mixins/validation.js');
const { handleRewardSet } = require('@/mixins/tradeIns.js');
const { log } = require('@/mixins/logging.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');
const Pool = require('@/models/tradeIns/pool.js');

module.exports = async (req, res) => {
  try {
    /* SETUP */
    const tradeIn = await TradeIn.findOne({ _id : req.params.id })
    .populate({
      path: 'conditions',
      populate : {
        path : 'items rewards',
        populate : {
          path : 'companion item',
        },
      },
    });

    if(!tradeIn)
      throw { name: 'NotFoundError' };

    const pool = await Pool.findOne({
      tradeIn,
      user: req.user
    })
    .populate({
      path: 'items'
    })

    if(!pool)
      throw { name: 'NotFoundError' };

    /* GIVE BACK ITEMS */
    await Promise.all([
      pool.items.map((userItem) => {
        userItem.user = req.user;
        userItem.save();
      })
    ]);

    /* PAYMENT */
    let newBalance;

    if(
      tradeIn.price > 0
    ) {
      newBalance = req.user[tradeIn.currency] + tradeIn.price;

      updatedUser[tradeIn.currency] = newBalance;
      await User.updateOne({ _id: req.user._id }, updatedUser);
    }

    await pool.remove();

    await log({
      tradeIn,
      user: req.user
    }, 'tradeIn', 'tradeInCancel', req.user);

    return res.json({
      newBalance,
      items: pool.items
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
