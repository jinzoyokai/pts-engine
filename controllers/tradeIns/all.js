const { handleValidationError } = require('@/mixins/validation.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await TradeIn.find({})
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
