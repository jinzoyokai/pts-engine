const fs = require('fs'),
      { handleValidationError } = require('@/mixins/validation.js');

const TradeIn = require('@/models/tradeIns/tradeIn.js')

module.exports = async (req, res) => {
  try {
    const tradeIn = await TradeIn.findById(req.params.id);

    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;

      if(tradeIn.icon)
        fs.unlink(tradeIn.icon, () => {
          // file deleted
        });
    }

    await TradeIn.updateOne({ _id: req.params.id }, payload)
    res.json(await TradeIn.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
