const { pick, omit } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js'),
      Character = require('@/models/character.js'),
      UserItem = require('@/models/userItem.js');

const UserPolicy = require('@/policies/character.js'),
      CharacterPolicy = require('@/policies/character.js'),
      UserItemPolicy = require('@/policies/userItem.js');

module.exports = async (req, res) => {
  try {
    const userItem = await UserItem.findById(req.params.id);

    const userItemPolicy = new UserItemPolicy(req.user, userItem);

    if(!userItemPolicy.canWrite())
      throw { name: 'PermissionError' };

    let payload = {
      ...omit(req.body, ['user', 'character'])
    };

    if(!!req.body.user) {
      const user = await User.findById(req.body.user);
      const userPolicy = new UserPolicy(req.user, user);

      if(!userPolicy.canWrite())
        throw { name: 'PermissionError' };

      payload.user = user._id;
    }

    if(!!req.body.character) {
      const character = await Character.findById(req.body.character);
      const characterPolicy = new CharacterPolicy(req.user, character);

      if(!characterPolicy.canWrite())
        throw { name: 'PermissionError' };

      payload.character = character._id;
    }

    payload = pick(payload, userItemPolicy.write());

    await UserItem.updateOne({ _id: req.params.id }, payload);
    res.json(await UserItem.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
