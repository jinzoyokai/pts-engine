const { pick, omit } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js'),
      UserItem = require('@/models/userItem.js');

const UserItemPolicy = require('@/policies/userItem.js');

module.exports = async (req, res) => {
  try {
    const userItem = await UserItem.findById(req.params.id).populate('item');

    const userItemPolicy = new UserItemPolicy(req.user, userItem);
    
    if(
      !userItemPolicy.canWrite()
      || !!userItem.character
      || !!userItem.soulbound
    )
      throw { name: 'PermissionError' };

    const user = await User.findById(userItem.user);

    const newBalance = user.currency + userItem.item.value;

    await userItem.remove();

    await User.updateOne({ _id: user._id }, {
      currency: newBalance
    });
    res.json({newBalance})
  } catch (error) {
    return handleValidationError(error, res);
  }
}
