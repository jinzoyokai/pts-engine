const { handleValidationError } = require('@/mixins/validation.js');

const UserItem = require('@/models/userItem.js');

const UserItemPolicy = require('@/policies/userItem.js');

module.exports = async (req, res) => {
  try {
    const userItem = await UserItem
      .findById(req.params.id)
      .populate('user')
      .populate('character');

    if(!userItem)
      throw { name: 'NotFoundError' };

    if(!userItemPolicy.canWrite())
      throw { name: 'PermissionError' };

    const userItemPolicy = new UserItemPolicy(req.user, userItem);

    res.json(userItem);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
