const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');

const UserItem = require('@/models/userItem.js');

module.exports = async (req, res) => {
  let userItem = new UserItem();

  userItem.item = req.body.item;
  userItem.user = req.body.user;
  userItem.character = req.body.character;
  userItem.soulbound = req.body.soulbound;

  try {
    await log(userItem, 'item', 'save', req.user);
    res.json(await userItem.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
