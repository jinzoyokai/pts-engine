const { handleValidationError } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');

const UserItem = require('@/models/userItem.js')

module.exports = async (req, res) => {
  try {
    const userItem = await UserItem.findById(req.params.id);

    if(!userItem)
      throw { name: 'NotFoundError' };

    await log(userItem, 'item', 'remove', req.user);
    res.json(await userItem.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
