const { handleValidationError } = require('@/mixins/validation.js');

const Trade = require('@/models/trade.js')

const TradePolicy = require('@/policies/trade.js');

module.exports = async (req, res) => {
  try {
    const trade = await Trade.findById(req.params.id)
                             .populate('from')
                             .populate('to')
                             .populate('items');

    if(!trade)
      throw { name: 'NotFoundError' };

    const tradePolicy = new TradePolicy(req.user, trade);

    if(!tradePolicy.canDelete())
      throw { name: 'PermissionError' };

    if(!!trade.fromCurrency) {
      trade.from.currency += parseInt(trade.fromCurrency);
      await trade.from.save();
    }

    if(!!trade.accepted && !!trade.toCurrency) {
      trade.to.currency += parseInt(trade.toCurrency);
      await trade.to.save();
    }

    if(
      !!trade.items
      && trade.items.length
    )
      for(const item of trade.items) {
        item.trade = null;
        await item.save();
      }

    await trade.remove()

    res.json({
      newBalance: trade.from.currency
    });
  } catch (error) {
    return handleValidationError(error, res)
  }
}
