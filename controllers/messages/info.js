const { handleValidationError } = require('@/mixins/validation.js');

const Thread = require('@/models/messageThread.js')

module.exports = async (req, res) => {
  try {
    let type = req.query.type || 'message';

    const result = await Thread.find({
      $or:[
        { from: req.user._id },
        { to: req.user._id }
      ],
      type,
    })

    res.json(result);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
