const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Thread = require('@/models/messageThread.js');

const ThreadPolicy = require('@/policies/messageThread.js');

module.exports = async (req, res) => {
  try {
    const thread = await Thread.findById(req.params.id)
                               .populate('from')
                               .populate('to');

   if(!thread)
     throw { name: 'NotFoundError' };

    const threadPolicy = new ThreadPolicy(req.user, thread);

    const payload = {};

    if(!threadPolicy.canWrite())
      throw { name: 'PermissionError' };

    if(threadPolicy.isTo())
      payload.toRead = true;

    if(threadPolicy.isFrom())
      payload.fromRead = true;

    await Thread.updateOne(
      { _id: req.params.id },
      payload
    );

    res.json({
      thread: await Thread.findById(req.params.id)
    })
  } catch (error) {
    return handleValidationError(error, res);
  }
}
