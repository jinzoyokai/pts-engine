const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { sendSystemMail } = require('@/mixins/messaging.js');

const Thread = require('@/models/messageThread.js');
const Message = require('@/models/message.js');
const User = require('@/models/user.js');


module.exports = async (req, res) => {
  const thread = new Thread();
  const message = new Message();

  const from = req.user._id;

  thread.from = from;
  thread.title = req.body.title;

  message.author = from;

  if(
    !req.body.content
  )
    return res.status(422).json({
      errors: ['"content" can not be empty.']
    });

  message.content = sanitize(req.body.content);

  const to = req.body.to;

  if(
    !to
    || to === req.user_id
  )
    return res.status(422).json({
      errors: ['"to" needs to be a valid user id.']
    });

  const toUser = await User.findById(to);

  if(!toUser)
    throw { name: 'NotFoundError' };

  thread.to = toUser;

  try {
    await thread.save();

    message.thread = thread;

    await message.save();
    await sendSystemMail('You received a new private message.', 'You received a new private message.', to, 'emailPMs')
    res.json({
      thread,
      message
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
