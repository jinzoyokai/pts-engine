const { handleValidationError } = require('@/mixins/validation.js');

const Thread = require('@/models/messageThread.js')

module.exports = async (req, res) => {
  try {
    let type = req.query.type || 'message';

    const result = await Thread.paginate({
      $or:[
        { from: req.user._id },
        { to: req.user._id }
      ],
      type,
    }, {
      populate: [{
        path: 'messages',
        options: { sort: { createdAt: -1 }},
      },
      {
        path: 'from',
        select: ['_id', 'username']
      },
      {
        path: 'to',
        select: ['_id', 'username']
      }],
      sort: '-updatedAt',
      page: req.query.page || 1,
      limit: 25
    });

    res.json(result);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
