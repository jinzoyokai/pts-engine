const { handleValidationError } = require('@/mixins/validation.js');

const Thread = require('@/models/messageThread.js');

const ThreadPolicy = require('@/policies/messageThread.js');

module.exports = async (req, res) => {
  try {
    const thread = await Thread.findById(req.params.id)
                               .populate('messages')
                               .populate({
                                 path: 'from',
                                 select: ['_id', 'username']
                               })
                               .populate({
                                 path: 'to',
                                 select: ['_id', 'username']
                               });

    if(!thread)
      throw { name: 'NotFoundError' };

    const threadPolicy = new ThreadPolicy(req.user, thread);


    if(!threadPolicy.canWrite())
      throw { name: 'PermissionError' };

    res.json(thread)
  } catch (error) {
    return handleValidationError(error, res);
  }
}
