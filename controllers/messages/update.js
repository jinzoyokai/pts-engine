const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { sendSystemMail } = require('@/mixins/messaging.js');

const Thread = require('@/models/messageThread.js');
const Message = require('@/models/message.js');

const ThreadPolicy = require('@/policies/messageThread.js');

module.exports = async (req, res) => {
  try {
    const thread = await Thread.findById(req.params.id)
                               .populate('from')
                               .populate('to');

   if(!thread)
     throw { name: 'NotFoundError' };

    const threadPolicy = new ThreadPolicy(req.user, thread);

    const payload = {};

    if(!threadPolicy.canWrite())
      throw { name: 'PermissionError' };

    if(threadPolicy.isTo())
      payload.fromRead = false;

    if(threadPolicy.isFrom())
      payload.toRead = false;


    const message = new Message();

    message.author = req.user._id;
    message.thread = thread;

    if(
      !req.body.content
    )
      return res.status(422).json({
        errors: ['"content" can not be empty.']
      });

    message.content = sanitize(req.body.content);

    payload.updatedAt = new Date();

    await Thread.updateOne(
      { _id: req.params.id },
      payload
    );

    await message.save();

    const to = threadPolicy.isTo() ? thread.from._id : thread.from._id;

    await sendSystemMail('You received a new private message.', 'You received a new private message.', to, 'emailPMs')

    res.json({
      thread: await Thread.findById(req.params.id),
      message
    })
  } catch (error) {
    return handleValidationError(error, res);
  }
}
