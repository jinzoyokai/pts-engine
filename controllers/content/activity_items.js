const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js')


module.exports = async (req, res) => {
  try {
    res.json(
      await Content.find(
        {
          category: 'activity',
          showInActivitiesTab: true,
        },
      ).select('title slug')
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
