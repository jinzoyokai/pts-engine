const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js');

module.exports = async (req, res) => {
  try {
    const content = await Content.find({ showInNav : true })
                                 .select('name navLabel slug');

    res.json(content);
  } catch (error) {
    return handleValidationError(error, res);
  }
}
