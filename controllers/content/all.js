const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js')

const UserPolicy = require('@/policies/user.js')

module.exports = async (req, res) => {
  const userPolicy = new UserPolicy(req.user)

  try {
    res.json(
      await Content.find({})
                   .populate('user', userPolicy.read())
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
