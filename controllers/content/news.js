const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js')

const UserPolicy = require('@/policies/user.js')

module.exports = async (req, res) => {
  const userPolicy = new UserPolicy(req.user)

  try {
    res.json(
      await Content.paginate(
        { category: 'news' },
        {
          page: req.query.page || 1,
          populate: {
            model: 'User',
            path: 'user',
            select: userPolicy.read()
          },
          sort: '-createdAt'
        }
      )
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
