const { handleValidationError } = require('@/mixins/validation.js');
const { sendSystemMessage } = require('@/mixins/messaging.js');

const Content = require('@/models/content.js');
const User = require('@/models/user.js');

module.exports = async (req, res) => {
  try {
    await Content.findById(req.params.id);
    await Content.updateOne({ _id: req.params.id }, req.body);

    if(!!req.body.notifyUsers) {
      // await User.update({}, { news: content._id });

      const users = await User.find({}).select('_id');

      for(const user of users) {
        sendSystemMessage(req.body.notificationMessage, req.body.notificationMessage, user._id, 'emailAnnouncements')
      }
    }

    res.json(await Content.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
