const { handleValidationError } = require('@/mixins/validation.js');
const { sendSystemMessage } = require('@/mixins/messaging.js');

const Content = require('@/models/content.js');
const User = require('@/models/user.js');

module.exports = async (req, res) => {
  let content = new Content();

  const existingContent = await Content.findOne({ slug: req.body.slug });

  if(existingContent)
    return res.status(422).json({
      errors: ['There\'s already content under that 🐌 slug.']
    });

  if(!!req.body.notifyUsers && !req.body.notificationMessage)
    return res.status(422).json({
      errors: ['Please provide a notification message']
    });

  content.slug = req.body.slug;
  content.title = req.body.title;
  content.category = req.body.category;
  content.hasComments = req.body.hasComments;
  content.showInActivitiesTab = req.body.showInActivitiesTab;
  content.showInNav = req.body.showInNav;

  content.notifyUsers = req.body.notifyUsers;
  content.notificationMessage = req.body.notificationMessage;

  content.navLabel = req.body.navLabel;
  content.body = req.body.body;
  content.user = req.user.id;

  try {
    await content.save()

    if(!!content.notifyUsers) {
      // await User.update({}, { news: content._id });

      const users = await User.find({}).select('_id');

      for(const user of users) {
        sendSystemMessage(req.body.notificationMessage, req.body.notificationMessage, user._id, 'emailAnnouncements')
      }
    }

    res.json(content);
  } catch (error) {
    return handleValidationError(error, res);
  }
}
