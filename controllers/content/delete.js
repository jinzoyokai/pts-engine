const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js')

module.exports = async (req, res) => {
  try {
    const content = await Content.findById(req.params.id);

    if(!content)
      throw { name: 'NotFoundError' };
      
    res.json(await content.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
