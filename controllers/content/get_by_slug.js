const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js');

const UserPolicy = require('@/policies/user.js');

module.exports = async (req, res) => {
  const userPolicy = new UserPolicy(req.user)

  try {
    const content = await Content.findOne({ slug : req.params.slug })
                                 .populate('user', userPolicy.read());

    if(!content)
      throw { name: 'NotFoundError' };

    res.json(content);
  } catch (error) {
    return handleValidationError(error, res);
  }
}
