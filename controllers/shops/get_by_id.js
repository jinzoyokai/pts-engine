const { handleValidationError } = require('@/mixins/validation.js');

const Shop = require('@/models/shop.js');


module.exports = async (req, res) => {
  try {
    const shop = await Shop.findById(req.params.id)
                           .populate({
                             path: 'inventory',
                             populate : {
                               path : 'item',
                             },
                           })
                           .populate({
                             path: 'companionInventory',
                             populate : {
                               path : 'companion',
                             },
                           });

    if(!shop)
      throw { name: 'NotFoundError' };

    res.json(shop);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
