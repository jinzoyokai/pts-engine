const { handleValidationError } = require('@/mixins/validation.js');

const Shop = require('@/models/shop.js')

module.exports = async (req, res) => {
  try {
    const shop = await Shop.findById(req.params.id);

    if(!shop)
      throw { name: 'NotFoundError' };

    res.json(await shop.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
