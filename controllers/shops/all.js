const { handleValidationError } = require('@/mixins/validation.js');

const Shop = require('@/models/shop.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await Shop.find({})
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
