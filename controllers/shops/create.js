const { handleValidationError } = require('@/mixins/validation.js');

const Shop = require('@/models/shop.js');

module.exports = async (req, res) => {
  let shop = new Shop();

  const existingShop = await Shop.findOne({ slug: req.body.slug });

  if(existingShop)
    return res.status(422).json({
      errors: ['There\'s already content under that 🐌 slug.']
    });

  shop.name = req.body.name;
  shop.slug = req.body.slug;
  shop.description = req.body.description;

  if(!!req.file && !!req.file.location)
    shop.icon = req.file.location;

  try {
    res.json(await shop.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
