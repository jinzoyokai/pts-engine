const fs = require('fs'),
      { handleValidationError } = require('@/mixins/validation.js');

const Shop = require('@/models/shop.js')

module.exports = async (req, res) => {
  try {
    const shop = await Shop.findById(req.params.id);

    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;

      if(shop.icon)
        fs.unlink(shop.icon, () => {
          // file deleted
        });
    }

    await Shop.updateOne({ _id: req.params.id }, payload)
    res.json(await Shop.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
