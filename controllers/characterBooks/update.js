const { pick, omit } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const CharacterBook = require('@/models/characterBook.js');


module.exports = async (req, res) => {
  try {
    const characterBook = await CharacterBook.findById(req.params.id);


    await CharacterBook.updateOne({ _id: req.params.id }, req.body);
    res.json(await CharacterBook.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
