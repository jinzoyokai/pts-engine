const { handleValidationError } = require('@/mixins/validation.js');

const CharacterBook = require('@/models/characterBook.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await CharacterBook.find({})
                         .populate('book')
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
