const { handleValidationError } = require('@/mixins/validation.js');

const CharacterBook = require('@/models/characterBook.js')

module.exports = async (req, res) => {
  try {
    const characterBook = await CharacterBook.findById(req.params.id);

    if(!characterBook)
      throw { name: 'NotFoundError' };

    res.json(await characterBook.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
