const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const CharacterBook = require('@/models/characterBook.js');

module.exports = async (req, res) => {
  let characterBook = new CharacterBook();

  characterBook.book = req.body.book;
  characterBook.character = req.body.character;

  try {
    res.json(await characterBook.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
