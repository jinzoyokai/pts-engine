const { pick } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const CharacterBook = require('@/models/characterBook.js');

module.exports = async (req, res) => {
  try {
    const characterBook = await CharacterBook.findById(req.params.id)
      .populate('book');

    if(!characterBook)
      throw { name: 'NotFoundError' };

    res.json(characterBook);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
