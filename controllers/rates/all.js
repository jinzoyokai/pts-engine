const { handleValidationError } = require('@/mixins/validation.js');

const Rate = require('@/models/rate.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Rate.find({})
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
