const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const ShopCompanion = require('@/models/shopCompanion.js');

module.exports = async (req, res) => {
  try {
    const shopCompanion = await ShopCompanion.findById(req.params.id);

    const updatedShopCompanion = req.body;

    await ShopCompanion.updateOne({ _id: req.params.id }, updatedShopCompanion)
    res.json(await ShopCompanion.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
