const { handleValidationError } = require('@/mixins/validation.js');

const ShopCompanion = require('@/models/shopCompanion.js')

module.exports = async (req, res) => {
  try {
    const shopCompanion = await ShopCompanion.findById(req.params.id);

    if(!shopCompanion)
      throw { name: 'NotFoundError' };

    res.json(await shopCompanion.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
