const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const ShopCompanion = require('@/models/shopCompanion.js'),
      Companion = require('@/models/companion.js'),
      Shop = require('@/models/shop.js');

module.exports = async (req, res) => {
  let shop,
      parent,
      shopCompanion = new ShopCompanion();

  try {
    shop = await Shop.findById(req.body.shop);
  } catch (error) {
    return handleValidationError(error, res)
  }

  try {
    companion = await Companion.findById(req.body.companion);
  } catch (error) {
    return handleValidationError(error, res)
  }

  shopCompanion.stock = req.body.stock;
  shopCompanion.unlimitedStock = req.body.unlimitedStock;
  shopCompanion.currency = req.body.currency;
  shopCompanion.price = req.body.price;
  shopCompanion.companion = companion._id;
  shopCompanion.shop = shop._id;

  try {
    res.json(await shopCompanion.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
