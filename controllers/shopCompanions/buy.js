const { pick } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');
const { getOtherSection } = require('@/mixins/users.js');

const ShopCompanion = require('@/models/shopCompanion.js');
const User = require('@/models/user.js');
const CharacterCompanion = require('@/models/characterCompanion.js');

module.exports = async (req, res) => {
  try {
    const shopCompanion = await ShopCompanion.findById(req.params.id);

    if(
      !shopCompanion.unlimitedStock
      && shopCompanion.stock === 0
    )
      return res.status(422).json({
        errors: ['This companion is sold out.']
      });



    if(req.user[shopCompanion.currency] < shopCompanion.price)
      return res.status(422).json({
        errors: ['You don\'t have sufficient funds.']
      });

    if(!shopCompanion.unlimitedStock)
      shopCompanion.stock = shopCompanion.stock - 1;


    const currencies = [
      'currency',
      'thorn_reputation_points',
      'aurora_faction_points',
      'eventide_faction_points',
      'casino_tickets',
      'pledge_points',
    ]

    if(currencies.indexOf(shopCompanion.currency) === -1)
      return res.status(422).json({
        errors: ['Shop companion not fully configured.']
      });

    const newBalance = req.user[shopCompanion.currency] - shopCompanion.price;

    const updatedUser = {};

    updatedUser[shopCompanion.currency] = newBalance;

    await Promise.all([
      User.updateOne({ _id: req.user._id }, updatedUser),
      ShopCompanion.updateOne({ _id: req.params.id }, shopCompanion)
    ]);

    const [updatedShopCompanion, user] = await Promise.all([
      ShopCompanion.findById(req.params.id),
      User.findById(req.user._id)
    ]);

    const otherSection = await getOtherSection(user);

    let characterCompanion = new CharacterCompanion();

    characterCompanion.companion = updatedShopCompanion.companion;
    characterCompanion.user = user._id;
    characterCompanion.section = otherSection._id;

    await log(shopCompanion, 'shopCompanion', 'save', req.user);

    res.json({
      characterCompanion: await characterCompanion.save(),
      shopCompanion: updatedShopCompanion,
      newBalance
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
