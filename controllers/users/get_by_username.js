const { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js');

const UserPolicy = require('@/policies/user.js');

module.exports = async (req, res) => {
  try {
    const userPolicy = new UserPolicy(req.user);

    const userRequest = User.findOne({
      username : {
         $regex : new RegExp(`^${req.params.username}$`, "i")
      }
    }
  )
                                     .select(userPolicy.read())
                                     .populate('rank')
                                     .populate({
                                       path: 'characters',
                                       populate : {
                                         path : 'companions',
                                         options: {
                                           sort: { 'order': 1 }
                                         },
                                         populate : {
                                           path : 'companion colorStatuses pokeball'
                                         }
                                       }
                                     })
                                     .populate({
                                       path: 'sections',
                                       populate : {
                                         path : 'companions',
                                         options: {
                                           sort: { 'order': 1 }
                                         },
                                         populate : {
                                           path : 'companion colorStatuses pokeball'
                                         }
                                       }
                                     })
                                     .populate({
                                       path: 'inventory',
                                       populate : {
                                         path : 'item'
                                       }
                                     });

    let user;

    if(String(req.params.username) === String(req.user.username)) {
      user = await userRequest.populate({
        path: 'submissions',
      });
    } else {
      user = await userRequest.populate({
        path: 'submissions',
        match: {
          'hidden' : false,
          'status' : 'approved'
        }
      });
    }

    if(!user)
      throw { name: 'NotFoundError' };

    res.json(user);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
