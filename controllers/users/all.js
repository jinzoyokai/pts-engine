const { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await User.findWithDeleted({})
                .populate('characters')
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
