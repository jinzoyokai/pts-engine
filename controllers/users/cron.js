const { handleValidationError, sanitize } = require('@/mixins/validation.js');

require('moment-timezone');

const moment = require('moment');
const schedule = require('node-schedule');

const User = require('@/models/user.js');

module.exports = (req, res) => {
  schedule.scheduleJob('0 15 0 * * *', async() => {
    const curTime = moment().toDate();

    const users = await User.findDeleted();

    for (let user of users) {
      if(user.deleted && user.deletedAt) {
        const d = moment(user.deletedAt).add(3, 'month').toDate();
        if(curTime >= d) {
          console.log(1)
          await user.remove();
        }

      }
    }
  });
}
