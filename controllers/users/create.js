const { hashSync } = require('bcrypt-nodejs');

const { handleValidationError } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const User = require('@/models/user.js');
const UserSection = require('@/models/userSection.js');

module.exports = async (req, res) => {
  let user = new User();

  user.username = req.body.username;
  user.currency = req.body.currency;
  user.thorn_reputation_points = req.body.thorn_reputation_points;
  user.aurora_faction_points = req.body.aurora_faction_points;
  user.eventide_faction_points = req.body.eventide_faction_points;
  user.casino_tickets = req.body.casino_tickets;
  user.pledge_points = req.body.pledge_points;
  user.dA = req.body.dA;
  user.discord = req.body.discord;
  user.bio = req.body.bio;
  user.rank = req.body.rank;
  user.permissions = req.body.permissions;
  user.achievements = req.body.achievements;
  user.hasTemporaryPassword = req.body.hasTemporaryPassword;
  user.password = !!req.body.password ? hashSync(req.body.password) : null;

  if(!!req.file && !!req.file.location)
    user.icon = req.file.location;



  try {
    await user.save()

    const customSection = new UserSection();
    customSection.name = 'Custom';
    customSection.user = user;
    await customSection.save();

    const otherSection = new UserSection();
    otherSection.name = 'Other';
    otherSection.user = user;
    await otherSection.save();

    res.json(user)
  } catch (error) {
    return handleValidationError(error, res)
  }
}
