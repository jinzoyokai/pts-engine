const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const User = require('@/models/user.js');

module.exports = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);
    const payload = req.body;

    await User.updateMany(
      {},
      payload
    );

    res.json({
      success: true
    })
  } catch (error) {
    return handleValidationError(error, res);
  }
}
