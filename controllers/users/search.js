const { handleValidationError, sanitizeRegex } = require('@/mixins/validation.js');

const User = require('@/models/user.js');

const UserPolicy = require('@/policies/user.js');

module.exports = async (req, res) => {
  const userPolicy = new UserPolicy(req.user);

  try {
    if(
      !req.query.username
      || req.query.username.length < 2
    )
      return res.status(422).json({
        errors: ['Please provide a valid search query.']
      });

    res.json(
      await User.find({
                  username: new RegExp(sanitizeRegex(req.query.username), 'i')
                })
                .select(userPolicy.read())
                .populate({
                  path: 'inventory',
                  match: {
                    soulbound: false,
                    trade: null
                  },
                  populate: {
                    path: 'item'
                  },

                })
                .populate({
                  path: 'companions',
                  select: ['_id', 'name', 'trade', 'user'],
                  populate: {
                    path: 'companion'
                  },
                })
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
