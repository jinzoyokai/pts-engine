const { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js')

module.exports = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    if(!user)
      throw { name: 'NotFoundError' };

    res.json(await user.delete());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
