const { hashSync } = require('bcrypt-nodejs');

const { pick } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { getPath } = require('@/mixins/files.js');
const { log } = require('@/mixins/logging.js');
const { currencyKeys } = require('@/common/currencies.js');

const User = require('@/models/user.js');
const Setting = require('@/models/setting.js');

const UserPolicy = require('@/policies/user.js');

module.exports = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    // const { value: activityCheck } = await Setting.findOne({ key: 'activity_check' });
    // const { value: modActivityCheck } = await Setting.findOne({ key: 'mod_activity_check' });
    //
    // const userPolicy = new UserPolicy(req.user, user, {
    //   activityCheck,
    //   modActivityCheck
    // });

    const userPolicy = new UserPolicy(req.user, user);

    if(!userPolicy.canWrite())
      throw { name: 'PermissionError' };

    let payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;
    } else {
      delete payload.icon;
    }

    if(!!payload.password)
      payload.password = hashSync(payload.password);

    if(payload.news !== undefined)
      payload.news = payload.news;

    if(!!payload.hiatus && !user.hiatus)
      payload.hiatusStartedAt = new Date();

    if(!!payload.action && payload.action == 'activityCheck')
      payload.activityCheckDate = new Date();

    if(!!payload.action && payload.action == 'modActivityCheck')
      payload.modActivityCheckDate = new Date();

    payload = pick(payload, userPolicy.write())

    if(payload.currency && payload.currency.startsWith('+')) {
      payload.currency = (user.currency || 0) + Number(payload.currency.substring(1))
    }

    await User.updateOne(
      { _id: req.params.id },
      payload,
      { runValidators: true }
    );

    const logCurrencyRequests = []

    for(let currency of currencyKeys) {
      console.log(currency)
      if(payload[currency])
        logCurrencyRequests.push(
          log(
            {
              ...payload,
              currencyKey: currency,
              _id: req.params.id
            },
            'user',
            'currency',
            req.user,
            user
          )
        )
    }

    await Promise.all(logCurrencyRequests)

    res.json(await User.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
