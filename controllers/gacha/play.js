const { pick } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');

const GachaItem = require('@/models/gachaItem.js');
const User = require('@/models/user.js');
const UserItem = require('@/models/userItem.js');

module.exports = async (req, res) => {
  try {
    const GACHA_PRICE = 5000;

    const gachaItems = await GachaItem.find({})
                                      .populate('item');

    if(req.user.currency < GACHA_PRICE)
      return res.status(422).json({
        errors: ['You don\'t have sufficient funds.']
      });


    const currency = req.user.currency - GACHA_PRICE;

    await User.updateOne({ _id: req.user._id }, { currency });

    const draw = () => {
        let weighted_items = [];

        for(item of gachaItems) {
          for(let i=0; i<item.weight; i++) {
              weighted_items.push(item);
          }
        }

        return weighted_items[Math.floor(Math.random() * weighted_items.length)] || items[0];
    };

    const result = draw();

    for (let i = 0; i < result.amount; i++) {
      let userItem = new UserItem();

      userItem.item = result.item;
      userItem.user = req.user._id;

      await userItem.save();
    }

    await log(result, 'game', 'gacha', req.user);

    res.json({
      newBalance: currency,
      gachaItem: result,
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
