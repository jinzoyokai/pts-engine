const { handleValidationError } = require('@/mixins/validation.js');

const GachaItem = require('@/models/gachaItem.js');

module.exports = async (req, res) => {
  try {
    const gachaItem = await GachaItem.findById(req.params.id);

    const updatedGachaItem = req.body;

    await GachaItem.updateOne({ _id: req.params.id }, updatedGachaItem)
    res.json(await GachaItem.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
