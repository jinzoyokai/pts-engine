const { handleValidationError } = require('@/mixins/validation.js');

const GachaItem = require('@/models/gachaItem.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await GachaItem.find({}).populate('item')
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
