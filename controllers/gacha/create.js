const { handleValidationError } = require('@/mixins/validation.js');

const GachaItem = require('@/models/gachaItem.js');
const Item = require('@/models/item.js');

module.exports = async (req, res) => {
  let shopItem = new GachaItem();

  try {
    item = await Item.findById(req.body.item);
  } catch (error) {
    return handleValidationError(error, res)
  }

  shopItem.item = item._id;
  shopItem.weight = req.body.weight;
  shopItem.amount = req.body.amount;

  try {
    res.json(await shopItem.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
