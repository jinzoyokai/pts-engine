const { handleValidationError } = require('@/mixins/validation.js');

const GachaItem = require('@/models/gachaItem.js')

module.exports = async (req, res) => {
  try {
    const gachaItem = await GachaItem.findById(req.params.id);

    if(!gachaItem)
      throw { name: 'NotFoundError' };

    res.json(await gachaItem.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
