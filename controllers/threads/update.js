const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Thread = require('@/models/thread.js');

const ThreadPolicy = require('@/policies/thread.js');

module.exports = async (req, res) => {
  try {
    const thread = await Thread.findById(req.params.id);

    const threadPolicy = new ThreadPolicy(req.user, thread);

    if(!threadPolicy.canWrite())
      throw { name : 'PermissionError'}

    const fields = threadPolicy.write();

    await Thread.updateOne({ _id: req.params.id }, pick(req.body, fields))
    res.json(await Thread.findById(req.params.id));
  } catch (error) {
    return handleValidationError(error, res)
  }
}
