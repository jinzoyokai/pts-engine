const { handleValidationError } = require('@/mixins/validation.js');

const Thread = require('@/models/thread.js');
const Post = require('@/models/post.js');

module.exports = async (req, res) => {
  let thread = await Thread.findById(req.params.id)
                             .populate('board');

  const posts = await Post.paginate({thread}, {
    page: req.query.page || 1,
    populate : {
      path : 'user',
      select: '_id username icon',
      populate: {
        path: 'rank'
      }
    }
  });

  const subscribed = thread.subscribers.indexOf(req.user.id) !== -1;

  try {
    res.json({
      thread: {
        ...thread.toJSON(),
        subscribed
      },
      posts
    });
  } catch (error) {
    return handleValidationError(error, res)
  }
}
