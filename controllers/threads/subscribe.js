const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Thread = require('@/models/thread.js');

module.exports = async (req, res) => {
  try {
    const thread = await Thread.findById(req.params.id);

    const subscribers = thread.subscribers || [];

    if(subscribers.indexOf(req.user._id) !== -1) {
      subscribers.splice(subscribers.indexOf(req.user._id), 1);
    } else {
      subscribers.push(req.user._id)
    }

    await Thread.updateOne({ _id: req.params.id }, {subscribers})
    res.json(await Thread.findById(req.params.id));
  } catch (error) {
    return handleValidationError(error, res)
  }
}
