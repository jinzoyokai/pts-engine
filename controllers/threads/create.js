const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Board = require('@/models/board.js');
const Thread = require('@/models/thread.js');
const Post = require('@/models/post.js');

const BoardPolicy = require('@/policies/board.js');

module.exports = async (req, res) => {
  const thread = new Thread();
  const post = new Post();

  try {
    board = await Board.findById(req.body.board);
    if(!board)
      throw { name: 'NotFoundError' };

    const boardPolicy = new BoardPolicy(req.user, board);

    if(!boardPolicy.canWrite())
      throw { name : 'PermissionError'}
  } catch (error) {
    return handleValidationError(error, res)
  }

  try {
    thread.title = sanitize(req.body.title);
    thread.user = req.user.id;
    thread.board = req.body.board;

    await thread.save();

    post.body = sanitize(req.body.body);
    post.user = req.user.id;
    post.thread = thread._id;
    post.board = req.body.board;

    await post.save()

    res.json({
      thread,
      post,
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
