const { handleValidationError } = require('@/mixins/validation.js');

const ShopItem = require('@/models/shopItem.js')

module.exports = async (req, res) => {
  try {
    const shopItem = await ShopItem.findById(req.params.id);

    if(!shopItem)
      throw { name: 'NotFoundError' };

    res.json(await shopItem.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
