const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const ShopItem = require('@/models/shopItem.js'),
      Item = require('@/models/item.js'),
      Shop = require('@/models/shop.js');

module.exports = async (req, res) => {
  let shop,
      parent,
      shopItem = new ShopItem();

  try {
    shop = await Shop.findById(req.body.shop);
  } catch (error) {
    return handleValidationError(error, res)
  }

  try {
    item = await Item.findById(req.body.item);
  } catch (error) {
    return handleValidationError(error, res)
  }

  shopItem.stock = req.body.stock;
  shopItem.unlimitedStock = req.body.unlimitedStock;
  shopItem.currency = req.body.currency;
  shopItem.price = req.body.price;
  shopItem.item = item._id;
  shopItem.shop = shop._id;

  try {
    res.json(await shopItem.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
