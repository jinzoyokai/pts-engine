const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const ShopItem = require('@/models/shopItem.js');

module.exports = async (req, res) => {
  try {
    const shopItem = await ShopItem.findById(req.params.id);

    const updatedShopItem = req.body;

    await ShopItem.updateOne({ _id: req.params.id }, updatedShopItem)
    res.json(await ShopItem.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
