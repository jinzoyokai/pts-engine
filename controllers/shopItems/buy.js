const { pick } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');
const { currencyKeys } = require('@/common/currencies.js');

const ShopItem = require('@/models/shopItem.js');
const User = require('@/models/user.js');
const UserItem = require('@/models/userItem.js');

module.exports = async (req, res) => {
  try {
    const shopItem = await ShopItem.findById(req.params.id);

    if(
      !shopItem.unlimitedStock
      && shopItem.stock === 0
    )
      return res.status(422).json({
        errors: ['This item is sold out.']
      });



    if(req.user[shopItem.currency] < shopItem.price)
      return res.status(422).json({
        errors: ['You don\'t have sufficient funds.']
      });

    if(!shopItem.unlimitedStock)
      shopItem.stock = shopItem.stock - 1;

    if(currencyKeys.indexOf(shopItem.currency) === -1)
      return res.status(422).json({
        errors: ['Shop item not fully configured.']
      });

    const newBalance = req.user[shopItem.currency] - shopItem.price;

    const updatedUser = {};

    updatedUser[shopItem.currency] = newBalance;

    await Promise.all([
      User.updateOne({ _id: req.user._id }, updatedUser),
      ShopItem.updateOne({ _id: req.params.id }, shopItem)
    ]);

    const [updatedShopItem, user] = await Promise.all([
      ShopItem.findById(req.params.id),
      User.findById(req.user._id)
    ]);

    let userItem = new UserItem();

    userItem.item = updatedShopItem.item;
    userItem.user = user._id;

    await log(shopItem, 'shopItem', 'save', req.user);

    res.json({
      userItem: await userItem.save(),
      shopItem: updatedShopItem,
      newBalance
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
