const { handleValidationError } = require('@/mixins/validation.js');

const File = require('@/models/file.js');

module.exports = async (req, res) => {

  try {
    const file = await File.findById(req.params.id);

    if(!file)
      throw { name: 'NotFoundError' };

    res.json(file);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
