const fs = require('fs'),
      { handleValidationError } = require('@/mixins/validation.js');

const User = require('@/models/user.js'),
      Character = require('@/models/character.js'),
      File = require('@/models/file.js');

const UserPolicy = require('@/policies/character.js'),
      CharacterPolicy = require('@/policies/character.js'),
      FilePolicy = require('@/policies/file.js');

module.exports = async (req, res) => {
  try {
    const file = await File.findById(req.params.id);

    const filePolicy = new FilePolicy(req.user, file);

    if(!filePolicy.canWrite())
      throw { name: 'PermissionError' };

    const payload = {};

    if(!!req.body.user) {
      const user = await User.findById(req.body.user);
      const userPolicy = new UserPolicy(req.user, user);

      if(!userPolicy.canWrite())
        throw { name: 'PermissionError' };

      payload.user = user._id;
    }

    if(!!req.body.character) {
      const character = await Character.findById(req.body.character);
      const characterPolicy = new CharacterPolicy(req.user, character);

      if(!characterPolicy.canWrite())
        throw { name: 'PermissionError' };

      payload.character = character._id;
    }

    if(!!req.body.key)
      payload.key = req.body.key;

    if(!!req.body.description)
      payload.description = req.body.description;

    if(!!req.file && !!req.file.location) {
      payload.file = req.file.location;

      fs.unlink(file.file, () => {
        // file deleted
      });
    }

    await File.updateOne({ _id: req.params.id }, payload);
    res.json(await File.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
