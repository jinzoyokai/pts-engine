const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const File = require('@/models/file.js'),
      User = require('@/models/user.js'),
      Character = require('@/models/character.js');

const UserPolicy = require('@/policies/character.js'),
      CharacterPolicy = require('@/policies/character.js');

module.exports = async (req, res) => {
  let file = new File();

  try {
    if(!req.body.user)
      return res.status(422).json({
        errors: ['Please provide a user.']
      });

    const user = await User.findById(req.body.user);
    const userPolicy = new UserPolicy(req.user, user);

    if(!userPolicy.canWrite())
      throw { name: 'PermissionError' };

    file.user = user._id;

    if(!!req.body.character) {
      const character = await Character.findById(req.params.id);
      const characterPolicy = new CharacterPolicy(req.user, character);

      if(!characterPolicy.canWrite())
        throw { name: 'PermissionError' };

      file.character = character._id;
    }

    file.key = req.body.key;
    file.description = req.body.description;

    if(!!req.file && !!req.file.location)
      file.file = req.file.location;


    res.json(await file.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
