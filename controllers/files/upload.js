const { handleValidationError, sanitizeRegex } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');


module.exports = async (req, res) => {
  try {
    res.end(req.file.location)
  } catch (error) {
    return handleValidationError(error, res)
  }
}
