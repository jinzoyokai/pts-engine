const { handleValidationError } = require('@/mixins/validation.js');

const Rank = require('@/models/rank.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Rank.find({})
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
