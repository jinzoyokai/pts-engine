const { handleValidationError } = require('@/mixins/validation.js');

const Rank = require('@/models/rank.js');

module.exports = async (req, res) => {

  try {
    const rank = await Rank.findById(req.params.id);

    if(!rank)
      throw { name: 'NotFoundError' };

    res.json(rank);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
