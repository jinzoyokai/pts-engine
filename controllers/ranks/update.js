const { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Rank = require('@/models/rank.js');

module.exports = async (req, res) => {
  try {

    const rank = await Rank.findById(req.params.id);
    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;
    } else {
      delete payload.icon;
    }

    await Rank.updateOne({ _id: req.params.id }, payload);
    res.json(await Rank.findById(req.params.id))
  } catch (error) {
    console.log(error)
    return handleValidationError(error, res);
  }
}
