const { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Rank = require('@/models/rank.js');

module.exports = async (req, res) => {
  let rank = new Rank();

  rank.name = req.body.name;

  if(!!req.file && !!req.file.location)
    rank.icon = req.file.location;

  try {
    res.json(await rank.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
