const { APP_URI } = require('@/config/app.js').constants;

const { handleValidationError } = require('@/mixins/validation.js');
const { tagUsers } = require('@/mixins/tagging.js');
const { sendSystemMessage } = require('@/mixins/messaging.js');

const Thread = require('@/models/thread.js');
const Post = require('@/models/post.js');

const BoardPolicy = require('@/policies/board.js');

module.exports = async (req, res) => {
  let thread,
      parent,
      post = new Post();

  try {
    thread = await Thread.findById(req.body.thread).populate('board');

    if(!thread || !!thread.closed)
      throw { name: 'NotFoundError' };

    const boardPolicy = new BoardPolicy(req.user, thread.board);

    if(!boardPolicy.canWrite())
      throw { name : 'PermissionError'}
  } catch (error) {
    return handleValidationError(error, res)
  }

  post.user = req.user.id;
  post.thread = thread._id;
  post.board = thread.board;
  post.body = await tagUsers(req.body.body, thread);

  const subscribed = thread.subscribers.indexOf(req.user.id) !== -1;

  if(!!thread.subscribers.length) {
    const requests = [];

    const title = 'A thread you subscribed to has a new post.'
    const link = `${APP_URI}/forums/${thread.board._id}/threads/${thread._id}`
    const message = `<a href="${link}">Click here to go the thread "${thread.title}"</a>`

    for(let subscriber of thread.subscribers) {
      if(String(req.user.id) !== String(subscriber)) {
        requests.push(sendSystemMessage(title, message, subscriber, 'emailSubscribedThread', { link }))
      }
    }


    await Promise.all(requests)
  }

  try {
    res.json(await post.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
