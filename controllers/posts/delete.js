const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Post = require('@/models/post.js');

const PostPolicy = require('@/policies/post.js');

module.exports = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    const postPolicy = new PostPolicy(req.user, post);

    if(!postPolicy.canWrite())
      throw { name : 'PermissionError'}

    res.json(await post.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
