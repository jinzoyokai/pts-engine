const { pick } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const { tagUsers } = require('@/mixins/tagging.js');

const Post = require('@/models/post.js');

const PostPolicy = require('@/policies/post.js');

module.exports = async (req, res) => {
  try {
    const post = await Post.findById(req.params.id);

    const postPolicy = new PostPolicy(req.user, post);

    if(!postPolicy.canWrite())
      throw { name : 'PermissionError'}

    const fields = postPolicy.write();

    const payload = pick(req.body, fields);

    if(!!payload.body)
      payload.body = await tagUsers(payload.body);

    await Post.updateOne({ _id: req.params.id }, payload);

    res.json(await Post.findById(req.params.id));
  } catch (error) {
    return handleValidationError(error, res)
  }
}
