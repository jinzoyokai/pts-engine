const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Setting = require('@/models/setting.js')

module.exports = async (req, res) => {
  let setting = new Setting();

  setting.key = req.body.key;
  setting.label = req.body.label;
  setting.value = req.body.value;
  setting.type = req.body.type;
  setting.group = req.body.group;

  try {
    res.json(await setting.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
