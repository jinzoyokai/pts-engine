const { handleValidationError } = require('@/mixins/validation.js');

const Setting = require('@/models/setting.js');

module.exports = async (req, res) => {

  try {
    const setting = await Setting.findOne({ key:req.params.key });

    if(!setting)
      throw { name: 'NotFoundError' };

    res.json(setting);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
