const { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Setting = require('@/models/setting.js');

module.exports = async (req, res) => {
  try {

    const setting = await Setting.findOne({ key: req.params.key });
    const payload = req.body;

    await Setting.updateOne({ key: req.params.key }, payload);
    res.json(await Setting.findById(setting._id))
  } catch (error) {
    console.log(error)
    return handleValidationError(error, res);
  }
}
