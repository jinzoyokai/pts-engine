const { handleValidationError } = require('@/mixins/validation.js');

const Setting = require('@/models/setting.js')

module.exports = async (req, res) => {
  let settings = await Setting.find({ group: req.params.group });

  settings = settings.map((setting) => {
    setting = setting.toObject();

    if(setting.type === 'Boolean')
      setting.value = setting.value === 'true';
      
    return setting;
  })
  try {
    res.json(settings);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
