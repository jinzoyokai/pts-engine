const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Comment = require('@/models/comment.js');

const CommentPolicy = require('@/policies/comment.js');

module.exports = async (req, res) => {
  try {
    const comment = await Comment.findById(req.params.id);

    const commentPolicy = new CommentPolicy(req.user, comment);

    if(!commentPolicy.canWrite())
      throw { name : 'PermissionError'}

    const fields = commentPolicy.write();

    const updatedComment = fields ? pick(req.body, fields) : req.body;

    await Comment.updateOne({ _id: req.params.id }, updatedComment)
    res.json(await Comment.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
