const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const Comment = require('@/models/comment.js');

const CommentPolicy = require('@/policies/comment.js');

module.exports = async (req, res) => {
  try {
    const comment = await Comment.findById(req.params.id);

    const commentPolicy = new CommentPolicy(req.user, Comment);

    if(!commentPolicy.canWrite())
      throw { name : 'PermissionError'}

    comment.body = '[deleted]';
    comment.softDeleted = new Date();

    res.json(await comment.save());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
