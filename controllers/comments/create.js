const { APP_URI } = require('@/config/app.js').constants;
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { sendSystemMessage } = require('@/mixins/messaging.js');

const Comment = require('@/models/comment.js'),
      Content = require('@/models/content.js'),
      Submission = require('@/models/submission.js');

module.exports = async (req, res) => {
  let parent,
      comment = new Comment();

  try {
    if(req.body.content) {
      const content = await Content.findById(req.body.content);
      if(!content || !content.hasComments)
        throw { name: 'NotFoundError' };

      comment.content = content._id;
    } else if(req.body.submission) {
      submission = await Submission.findById(req.body.submission).populate('user');
      if(!submission)
        throw { name: 'NotFoundError' };

      comment.submission = submission._id;

    } else {
      throw { name: 'NotFoundError' };
    }
  } catch (error) {
    return handleValidationError(error, res)
  }

  comment.body = sanitize(req.body.body);
  comment.user = req.user.id;

  if(req.body.parent) {
    try {
      let parentComment = await Comment.findById(req.body.parent);
      if(!parentComment)
        throw { name: 'NotFoundError' };

      comment.parent = parentComment;
    } catch (error) {
      return handleValidationError(error, res)
    }
  }

  try {
    if(
      req.body.submission
      && String(submission.user._id) !== String(req.user.id)
    ) {
      const link = `${APP_URI}/u/${submission.user.username}#gallery-${submission.id}`;

      let body = `You received a new comment from "${req.user.username}" on your submission: `;
      body += `<a href="${link}">Link</a>`
      sendSystemMessage('Your submission has a new comment', body, submission.user._id, 'emailSubmissionComments', {link})
    }

    res.json(await comment.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
