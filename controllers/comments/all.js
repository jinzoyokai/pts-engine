const { handleValidationError } = require('@/mixins/validation.js');

const Comment = require('@/models/comment.js');

const UserPolicy = require('@/policies/user.js');

module.exports = async (req, res) => {
  const userPolicy = new UserPolicy(req.user);

  try {
    res.json(
      await Comment.find({})
                         .populate('user', userPolicy.read())
                         .populate('content')
                         .populate('parent')
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
