const { handleValidationError } = require('@/mixins/validation.js');

const Content = require('@/models/content.js');

module.exports = async (req, res) => {

  try {
    const content = await Content.findById(req.params.id)
                                 .populate({
                                   path: 'comments',
                                   populate : {
                                     path : 'user',
                                     select: ['_id', 'username']
                                   },
                                 }).lean();

    if(!content)
      throw { name: 'NotFoundError' };

    const comments = content.comments;

    function sortByCreatedAt(a, b) { return b.createdAt - a.createdAt };

    function getChildComments(topLevelComment) {
      const childComments = comments.filter(({parent}) => !!parent)
                                    .sort(sortByCreatedAt)
                                    .filter(({parent}) => topLevelComment._id.equals(parent))

                                    .map((comment) => {
                                      const childComments = getChildComments(comment);

                                      if(childComments.length)
                                        comment.comments = getChildComments(comment);

                                      return comment;
                                    });

      if(childComments.length)
        topLevelComment.comments = childComments;

      return topLevelComment;
    };

    const topLevelComments = comments.filter(({parent}) => !parent)
                                  .sort(sortByCreatedAt)
                                     .map((comment) => getChildComments(comment));

    res.json(topLevelComments);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
