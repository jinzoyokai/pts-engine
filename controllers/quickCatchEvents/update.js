const { handleValidationError } = require('@/mixins/validation.js');
const { getPath } = require('@/mixins/files.js');

const QuickCatchEvent = require('@/models/quickCatchEvent.js');

const QuickCatchEventPolicy = require('@/policies/quickCatchEvent.js');

module.exports = async (req, res) => {
  try {
    const quickCatchEvent = await QuickCatchEvent.findById(req.params.id);

    const quickCatchEventPolicy = new QuickCatchEventPolicy(req.user, quickCatchEvent);

    if(!quickCatchEventPolicy.canWrite())
      throw { name: 'PermissionError' };

    let payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.image = req.file.location;
    } else {
      delete payload.image;
    }

    await QuickCatchEvent.updateOne(
      { _id: req.params.id },
      payload
    );

    res.json(await QuickCatchEvent.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
