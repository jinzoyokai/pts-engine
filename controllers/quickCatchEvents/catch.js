const { hashSync } = require('bcrypt-nodejs');

const { pick } = require('lodash');
const { handleValidationError } = require('@/mixins/validation.js');
const { getPath } = require('@/mixins/files.js');
const { getOtherSection } = require('@/mixins/users.js');

const QuickCatchEvent = require('@/models/quickCatchEvent.js');
const UserItem = require('@/models/userItem.js');
const CharacterCompanion = require('@/models/characterCompanion.js');

const QuickCatchEventPolicy = require('@/policies/quickCatchEvent.js');

module.exports = async (req, res) => {
  try {
    const quickCatchEvent = await QuickCatchEvent.findById(req.params.id)
                                                 .populate('companion')
                                                 .populate('pokeball');

    if(
      !req.body.pokeball
    )
      return res.status(422).json({
        errors: ['Catch failed. No Pokeball was thrown.']
      });

    const pokeball = await UserItem.findById(req.body.pokeball)
                                   .populate('item');

    const quickCatchEventPolicy = new QuickCatchEventPolicy(req.user, quickCatchEvent);

    if(
      !quickCatchEventPolicy.canWrite()
      || pokeball.item.modifier < quickCatchEvent.pokeball.modifier
      || String(pokeball.user) !== String(req.user._id)
    )
      throw { name: 'PermissionError' };

    const characterCompanion = new CharacterCompanion();

    characterCompanion.level = 1;
    characterCompanion.pokeball = pokeball.item;
    characterCompanion.companion = quickCatchEvent.companion;
    characterCompanion.image = quickCatchEvent.image;
    characterCompanion.user = req.user;
    characterCompanion.section = await getOtherSection(req.user._id);

    await characterCompanion.save();
    await pokeball.remove();

    await QuickCatchEvent.updateOne(
      { _id: req.params.id },
      {
        status: 'caught',
        user: req.user,
        caughtAt: new Date(),
      }
    );

    res.json({
      quickCatchEvent: await QuickCatchEvent.findById(req.params.id),
      characterCompanion,
    });
  } catch (error) {
    return handleValidationError(error, res);
  }
}
