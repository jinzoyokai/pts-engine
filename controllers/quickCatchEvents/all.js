const { handleValidationError } = require('@/mixins/validation.js');

const QuickCatchEvent = require('@/models/quickCatchEvent.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await QuickCatchEvent.find({})
                           .populate('companion')
                           .populate('pokeball')
                           .populate('user')
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
