const { handleValidationError } = require('@/mixins/validation.js');

const QuickCatchEvent = require('@/models/quickCatchEvent.js')

module.exports = async (req, res) => {
  try {
    const quickCatchEvent = await QuickCatchEvent.findById(req.params.id);

    if(!quickCatchEvent)
      throw { name: 'NotFoundError' };

    res.json(await quickCatchEvent.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
