const { handleValidationError, sanitize } = require('@/mixins/validation.js');

require('moment-timezone');

const moment = require('moment');
const schedule = require('node-schedule');

const QuickCatchEvents = require('@/models/quickCatchEvent.js');

module.exports = (req, res) => {
  schedule.scheduleJob('*/10 * * * * *', async() => {
    const curTime = moment().tz('America/Toronto').toDate();
    const curTimePlus30 = moment().tz('America/Toronto').add(30, 'm').toDate();

    const quickCatchEvents = await QuickCatchEvents.find({ $or:[ {status : 'scheduled'}, {status : 'wild'} ]})
                            .populate('companion')
                            .populate('pokeball');

    for (let quickCatchEvent of quickCatchEvents) {
      const d = new Date(quickCatchEvent.schedule);

      if(quickCatchEvent.status === 'scheduled') {
        if(curTime >= d) {
          quickCatchEvent.status = 'wild';
          await quickCatchEvent.save();
        }
      } else if (quickCatchEvent.status === 'wild') {
        if(curTimePlus30 >= d) {
          quickCatchEvent.status = 'pityball';
          await quickCatchEvent.save();
        }
      }
    }

  });
}
