const { handleValidationError } = require('@/mixins/validation.js');

const QuickCatchEvent = require('@/models/quickCatchEvent.js');

module.exports = async (req, res) => {
  const quickCatchEvent = new QuickCatchEvent();

  quickCatchEvent.companion = req.body.companion;
  quickCatchEvent.pokeball = req.body.pokeball;
  quickCatchEvent.image = req.body.image;
  quickCatchEvent.urlMatch = req.body.urlMatch;
  quickCatchEvent.schedule = req.body.schedule;

  if(!!req.file && !!req.file.location)
    quickCatchEvent.image = req.file.location;

  try {
    res.json(await quickCatchEvent.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
