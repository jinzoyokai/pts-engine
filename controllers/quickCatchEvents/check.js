const { handleValidationError } = require('@/mixins/validation.js');

const QuickCatchEvent = require('@/models/quickCatchEvent.js');

const QuickCatchEventPolicy = require('@/policies/quickCatchEvent.js');

module.exports = async (req, res) => {
  try {
    let quickCatchEvent;

    const url = req.body.url;

    const quickCatchEvents = await QuickCatchEvent.find({ $or:[ {status : 'wild'}, {status : 'pityball'} ]})
                                                  .select('_id urlMatch');

    const result = quickCatchEvents.find(({urlMatch}) => new RegExp(urlMatch, 'i').test(url));

    if(!!result)
      quickCatchEvent = await QuickCatchEvent.findById(result._id)
                                             .populate('companion')
                                             .populate('pokeball');

    res.json(quickCatchEvent);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
