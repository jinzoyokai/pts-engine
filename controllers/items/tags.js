const { uniq, flatten } = require('lodash');

const { handleValidationError } = require('@/mixins/validation.js');

const Item = require('@/models/item.js')

module.exports = async (req, res) => {
  try {
    const items = await Item.find({});

    const tags = uniq(
                  flatten(
                    items
                      .map(item => item.tags)
                      .filter(item => !!item && Array.isArray(item))
                  )
    );

    res.json(tags);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
