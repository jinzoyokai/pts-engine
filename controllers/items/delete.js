const { handleValidationError } = require('@/mixins/validation.js');

const Item = require('@/models/item.js')

module.exports = async (req, res) => {
  try {
    const item = await Item.findById(req.params.id);

    if(!item)
      throw { name: 'NotFoundError' };

    res.json(await item.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
