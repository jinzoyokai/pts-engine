const { uniq } = require('lodash');

const { handleValidationError } = require('@/mixins/validation.js');

const Item = require('@/models/item.js')

module.exports = async (req, res) => {
  try {
    const items = await Item.find({});

    const categories = uniq(
                  items
                    .map(item => item.category)
                    .filter(item => !!item)
    );

    res.json(categories);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
