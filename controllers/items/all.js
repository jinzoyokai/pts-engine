const { handleValidationError } = require('@/mixins/validation.js');

const Item = require('@/models/item.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Item.find({})
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
