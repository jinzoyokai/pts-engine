const { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Item = require('@/models/item.js');

module.exports = async (req, res) => {
  try {

    const item = await Item.findById(req.params.id);
    const payload = req.body;

    if(!!req.file && !!req.file.location) {
      payload.icon = req.file.location;
    } else {
      delete payload.icon;
    }

    await Item.updateOne({ _id: req.params.id }, payload);
    res.json(await Item.findById(req.params.id))
  } catch (error) {
    console.log(error)
    return handleValidationError(error, res);
  }
}
