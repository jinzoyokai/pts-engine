const { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Item = require('@/models/item.js')

module.exports = async (req, res) => {
  let item = new Item();

  item.name = req.body.name;
  item.category = req.body.category;
  item.description = req.body.description;
  item.rarity = req.body.rarity;
  item.value = req.body.value;
  item.modifier = req.body.modifier;
  item.tags = req.body.tags;

  if(!!req.file && !!req.file.location)
    item.icon = req.file.location;

  try {
    res.json(await item.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
