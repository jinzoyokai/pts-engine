const { handleValidationError } = require('@/mixins/validation.js');

const Character = require('@/models/character.js');

module.exports = async (req, res) => {
  try {
    const character = await Character.findById(req.params.id)
                                     .populate('files')
                                     .populate('user', ['username'])
                                     .populate('rank')
                                     .populate({
                                       path: 'companions',
                                       populate : {
                                         path : 'companion colorStatuses pokeball'
                                       }
                                     })
                                     .populate({
                                       path: 'inventory',
                                       populate : {
                                         path : 'item'
                                       }
                                     });

    if(!character)
      throw { name: 'NotFoundError' };

    res.json(character);
  } catch (error) {
    return handleValidationError(error, res)
  }
}
