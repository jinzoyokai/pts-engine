const { handleValidationError } = require('@/mixins/validation.js');

const Character = require('@/models/character.js')

module.exports = async (req, res) => {
  try {
    const character = await Character.findById(req.params.id);

    if(!character)
      throw { name: 'NotFoundError' };

    res.json(await character.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
