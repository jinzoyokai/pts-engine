const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Character = require('@/models/character.js');

const CharacterPolicy = require('@/policies/character.js');

module.exports = async (req, res) => {
  try {
    const character = await Character.findById(req.params.id);

    const characterPolicy = new CharacterPolicy(req.user, character);

    if(!characterPolicy.canWrite())
      throw { name: 'PermissionError' };

    const payload = req.body;

    if(
      !!req.files
      && !!req.files.profileImage
      && !!req.files.profileImage.length
    ) {
      payload.profileImage = req.files.profileImage[0].location;
    } else {
      delete payload.profileImage;
    }

    if(
      !!req.files
      && !!req.files.design
      && !!req.files.design.length
    ) {
      payload.design = req.files.design[0].location;
    } else {
      delete payload.design;
    }

    if(payload.profile)
      payload.profile = payload.profile;

    await Character.updateOne(
      { _id: req.params.id },
      pick(payload, characterPolicy.write())
    );
    res.json(await Character.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
