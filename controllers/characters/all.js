const { handleValidationError } = require('@/mixins/validation.js');

const Character = require('@/models/character.js')

module.exports = async (req, res) => {
  try {
    res.json(
      await Character.find({user: {$exists: true}})
                     .populate('user', ['username'])
    );
  } catch (error) {
    return handleValidationError(error, res)
  }
}
