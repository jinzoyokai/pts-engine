const { handleValidationError, sanitize } = require('@/mixins/validation.js'),
      { getPath } = require('@/mixins/files.js');

const Character = require('@/models/character.js')

module.exports = async (req, res) => {
  let character = new Character();

  character.name = req.body.name;
  character.job = req.body.job;
  character.gender = req.body.gender;
  character.pronouns = req.body.pronouns;
  character.nature = req.body.nature;
  character.status = req.body.status;
  character.teams = req.body.teams;
  character.profile = req.body.profile ? sanitize(req.body.profile) : undefined;
  character.adminNotes = req.body.adminNotes;
  character.specialty = req.body.specialty;

  character.user = req.body.user;

  if(
    !!req.files
    && !!req.files.profileImage
    && !!req.files.profileImage.length
  )
    character.profileImage = req.files.profileImage[0].location;

  if(
    !!req.files
    && !!req.files.design
    && !!req.files.design.length
  )
    character.design = req.files.design[0].location;

  try {
    res.json(await character.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
