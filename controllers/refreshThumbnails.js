const Submission = require('@/models/submission.js');

module.exports = async (req, res) => {
  const submissions = await Submission.find({});

  for(const submission of submissions) {
    await submission.generateThumbnail();
  }

  return res.end();
}
