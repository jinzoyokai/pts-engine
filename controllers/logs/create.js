const { handleValidationError } = require('@/mixins/validation.js');

const Log = require('@/models/log.js');
const User = require('@/models/user.js');

module.exports = async (req, res) => {


  try {
    const user = await User.findById(req.params.id);

    if(!user)
      throw { name: 'NotFoundError' };

    let log = new Log();

    log.type = 'custom';
    log.action = 'custom';
    log.message = req.body.message;
    log.user = user;
    log.staff = req.user;

    res.json(await log.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
