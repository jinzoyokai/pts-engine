const { handleValidationError } = require('@/mixins/validation.js');

const Log = require('@/models/log.js');
const User = require('@/models/user.js');

const UserPolicy = require('@/policies/user.js');

module.exports = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    const userPolicy = new UserPolicy(req.user, user);

    if(!userPolicy.canReadLogs())
      throw { name: 'PermissionError' };

    const log = await Log.find({ user: req.params.id })
                         .sort('-createdAt')
                         .populate('user', ['_id', 'username'])
                         .populate('staff', ['_id', 'username']);

    res.json(log)
  } catch (error) {
    return handleValidationError(error, res);
  }
}
