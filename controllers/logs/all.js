const { handleValidationError } = require('@/mixins/validation.js');

const Log = require('@/models/log.js');

module.exports = async (req, res) => {
  try {
    res.json(
        await Log.find({})
                 .sort('-createdAt')
                 .populate('user', ['_id', 'username'])
                 .populate('staff', ['_id', 'username'])
    )
  } catch (error) {
    return handleValidationError(error, res);
  }
}
