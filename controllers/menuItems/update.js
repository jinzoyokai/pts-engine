const fs = require('fs'),
      { handleValidationError } = require('@/mixins/validation.js');

const MenuItem = require('@/models/menuItem.js')

module.exports = async (req, res) => {
  try {
    const menuItem = await MenuItem.findById(req.params.id);

    const payload = req.body;

    await MenuItem.updateOne({ _id: req.params.id }, payload)
    res.json(await MenuItem.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
