const { handleValidationError } = require('@/mixins/validation.js');

const MenuItem = require('@/models/menuItem.js');

module.exports = async (req, res) => {
  let menuItem = new MenuItem();

  menuItem.label = req.body.label;
  menuItem.url = req.body.url;
  menuItem.icon = req.body.icon;
  menuItem.authOnly = req.body.authOnly;
  menuItem.children = req.body.children;

  try {
    res.json(await menuItem.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
