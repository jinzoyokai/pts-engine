const { handleValidationError } = require('@/mixins/validation.js');

const MenuItem = require('@/models/menuItem.js')

module.exports = async (req, res) => {
  try {
    const menuItem = await MenuItem.findById(req.params.id);

    if(!menuItem)
      throw { name: 'NotFoundError' };

    res.json(await menuItem.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
