const { handleValidationError } = require('@/mixins/validation.js');

const MenuItem = require('@/models/menuItem.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await MenuItem.find({}).sort({
        order: 1
      })
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
