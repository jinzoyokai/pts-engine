const { handleValidationError } = require('@/mixins/validation.js');

const UserSection = require('@/models/userSection.js')

module.exports = async (req, res) => {
  try {
    const userSection = await UserSection
      .findById(req.params.id)
      .populate('companions');

    if(!userSection)
      throw { name: 'NotFoundError' };

    if(userSection.companions.length > 0)
      return res.status(422).json({
        errors: ['There are still characters assigned to this section.']
      });

    res.json(await userSection.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
