const { pick } = require('lodash'),
      { handleValidationError } = require('@/mixins/validation.js');

const UserSection = require('@/models/userSection.js');

module.exports = async (req, res) => {
  try {
    const userSection = await UserSection.findById(req.params.id);;

    payload = pick(payload, userSectionPolicy.write());

    await UserSection.updateOne({ _id: req.params.id }, payload);
    res.json(await UserSection.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res);
  }
}
