const { handleValidationError } = require('@/mixins/validation.js');

const UserSection = require('@/models/userSection.js');

module.exports = async (req, res) => {
  let userSection = new UserSection();

  userSection.name = req.body.name;
  userSection.user = req.body.user;

  try {
    res.json(await userSection.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
