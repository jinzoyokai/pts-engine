module.exports = (req, res) => {
  req.session.entry = req.headers['referer'];

  return res.redirect('/engine/auth/provider/deviantart/redirect');
}
