const { handleValidationError } = require('@/mixins/validation.js');

const TossPrize = require('@/models/tossPrize.js')

module.exports = async (req, res) => {
  try {
    const tossPrize = await TossPrize.findById(req.params.id);

    if(!tossPrize)
      throw { name: 'NotFoundError' };

    res.json(await tossPrize.remove());
  } catch (error) {
    return handleValidationError(error, res)
  }
}
