const { pick } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');
const { getOtherSection } = require('@/mixins/users.js');

const TossPrize = require('@/models/tossPrize.js');
const User = require('@/models/user.js');
const UserItem = require('@/models/userItem.js');
const CharacterCompanion = require('@/models/characterCompanion.js');

module.exports = async (req, res) => {
  try {
    if(
      !req.body.pokeball
    )
      return res.status(422).json({
        errors: ['Catch failed. No Pokeball was thrown.']
      });

    const pokeball = await UserItem.findById(req.body.pokeball)
                                   .populate('item');

     if(String(pokeball.user) !== String(req.user._id))
       throw { name: 'PermissionError' };

    const tossPrizes = await TossPrize.find({
      pokeball: pokeball.item._id
    });

    const draw = () => {
        let weighted_items = [];

        for(item of tossPrizes) {
          for(let i=0; i<item.weight; i++) {
              weighted_items.push(item);
          }
        }

        return weighted_items[Math.floor(Math.random() * weighted_items.length)] || items[0];
    };

    const result = draw();

    for (let i = 0; i < result.items; i++) {
      let userItem = new UserItem();

      userItem.item = result.items[i];
      userItem.user = req.user._id;

      await userItem.save();
    }

    if(result.pokemon.length) {
      const otherSection = await getOtherSection(req.user._id);

      for (let i = 0; i < result.pokemon; i++) {
        const characterCompanion = new CharacterCompanion();

        characterCompanion.level = 1;
        characterCompanion.pokeball = pokeball.item;
        characterCompanion.companion = result.pokemon[0].companion;
        characterCompanion.user = req.user;
        characterCompanion.section = otherSection;

        await characterCompanion.save();
      }
    }

    await pokeball.remove();

    await log(result, 'game', 'gacha', req.user);

    res.json({
      newBalance: currency,
      gachaItem: result,
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
