const { handleValidationError } = require('@/mixins/validation.js');

const TossPrize = require('@/models/tossPrize.js');
const Companion = require('@/models/companion.js');
const Item = require('@/models/item.js');

module.exports = async (req, res) => {
  const tossPrize = new TossPrize();

  tossPrize.pokeball = req.body.pokeball;
  tossPrize.weight = req.body.weight;

  tossPrize.items = [];

  if(req.body.items) {
    try {
      for (const itemId of req.body.items) {
        const item = await Item.findById(itemId);

        tossPrize.items.push(item);
      }
    } catch (error) {
      return handleValidationError(error, res)
    }
  }

  tossPrize.pokemon = [];

  if(req.body.pokemon) {
    try {
      for (const itemId of req.body.pokemon) {
        const companion = await Companion.findById(itemId);

        tossPrize.pokemon.push(companion);
      }
    } catch (error) {
      return handleValidationError(error, res)
    }
  }

  try {
    res.json(await tossPrize.save())
  } catch (error) {
    return handleValidationError(error, res)
  }
}
