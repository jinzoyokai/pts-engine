const { pick } = require('lodash'),
      { handleValidationError, sanitize } = require('@/mixins/validation.js');

const TossPrize = require('@/models/tossPrize.js');

module.exports = async (req, res) => {
  try {
    const tossPrize = await TossPrize.findById(req.params.id);

    const updatedTossPrize = req.body;

    await TossPrize.updateOne({ _id: req.params.id }, updatedTossPrize)
    res.json(await TossPrize.findById(req.params.id))
  } catch (error) {
    return handleValidationError(error, res)
  }
}
