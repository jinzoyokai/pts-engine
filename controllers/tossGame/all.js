const { handleValidationError } = require('@/mixins/validation.js');

const TossPrize = require('@/models/tossPrize.js');

module.exports = async (req, res) => {
  try {
    res.json(
      await TossPrize.find({})
    );
  } catch (error) {
    return handleValidationError(error, res);
  }
}
