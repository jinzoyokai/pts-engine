const { handleValidationError } = require('@/mixins/validation.js'),
      { generatePassword } = require('@/mixins/auth.js');

const User = require('@/models/user.js');
const Setting = require('@/models/setting.js');

module.exports = async (req, res) => {
  let user = new User();
  let setting = new Setting();
  let setting2 = new Setting();

  const existingUser = await User.findOne({ username: 'neighbordog' });

  if(existingUser)
    res.status(422).json({
      errors: ['An user with that username already exists.']
    });

  const existingSetting = await User.findOne({ key: 'home' });

  if(existingSetting)
    res.status(422).json({
      errors: ['This setting already exists.']
    });

  user.username = 'neighbordog';
  user.password = generatePassword('safran');
  user.bio = 'icecream';
  user.adminNotes = '';
  user.permissions = ['admin', 'author', 'moderate', 'can_edit_users'];

  setting.key = 'home';
  setting.label = 'Home';
  setting.value = 'Change home settings under admin › settings.';
  setting.type = 'Text';
  setting.group = 'home';

  setting2.key = 'contact';
  setting2.label = 'Admin';
  setting2.value = 'none';
  setting2.type = 'Text';
  setting2.group = 'app';

  try {
    const result = await Promise.all([
      user.save(),
      setting.save(),
      setting2.save()
    ])
    res.json(result)
  } catch (error) {
    return handleValidationError(error, res)
  }
}
