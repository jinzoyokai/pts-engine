const { pick } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { log } = require('@/mixins/logging.js');

const GachaItem = require('@/models/gachaItem.js');
const User = require('@/models/user.js');
const UserItem = require('@/models/userItem.js');



module.exports = async (req, res) => {
  try {
    const ICONS = {
        0 : {
            name : '7',
            key : 0,
            image : null,
            payout : 35000,
            weight : 1
        },
        1 : {
            name : 'R',
            key : 1,
            image : null,
            payout : 3500,
            weight : 2
        },
        2 : {
            name : 'Pikachu',
            key : 2,
            image : null,
            payout : 2500,
            weight : 3
        },
        3 : {
            name : 'Enton',
            key : 3,
            image : null,
            payout : 2000,
            weight : 4
        },
        4 : {
            name : 'MagnettiSpaghetti',
            key : 4,
            image : null,
            payout : 1000,
            weight : 5
        },
        5 : {
            name : 'Muschias',
            key : 5,
            image : null,
            payout : 500,
            weight : 6
        },
        6 : {
            name : 'PekeKirsch',
            key : 6,
            image : null,
            payout : 100,
            weight : 8
        }
    };

    let sumOfWeights = 0;

    Object.keys(ICONS).forEach(function(key) {
        let icon = ICONS[key];

        sumOfWeights += icon.weight;
    });

    const draw = function() {
        let random = Math.floor(Math.random() * (sumOfWeights + 1));
        const result = Object.keys(ICONS).find(function(key) {
            random -= ICONS[key].weight;
            return random <= 0;
        });

        return ICONS[result] || ICONS[6];
    };

    const SLOTS_PRICE = 500;

    const gachaItems = await GachaItem.find({})
                                      .populate('item');

    if(req.user.currency < SLOTS_PRICE)
      return res.status(422).json({
        errors: ['You don\'t have sufficient funds.']
      });


    const currency = req.user.currency - SLOTS_PRICE;

    await User.updateOne({ _id: req.user._id }, { currency });


    let result;
    const history = [];

    for(let i = 0; i < 3; i++) {
        result = draw();

        ICONS[result.key].weight *= 4.5;

        history.push(result);
    }

    const win = (history[0] == history[1] && history[1] == history[2]);

    if(!!win)
      await User.updateOne({ _id: req.user._id }, {
        currency : currency + history[0].payout
      });

    res.json({
      win,
      newBalance: currency,
      history
    })
  } catch (error) {
    return handleValidationError(error, res)
  }
}
