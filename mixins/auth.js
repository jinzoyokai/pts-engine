bcrypt = require('bcrypt-nodejs');

module.exports.generatePassword = (plaintext) => {
  return bcrypt.hashSync(plaintext, bcrypt.genSaltSync())
}
