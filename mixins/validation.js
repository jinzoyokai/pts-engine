const { assign } = require('lodash'),
      sanitize = require('sanitize-html');

module.exports.handleValidationError = (error, res) => {
  if(error.name === 'PermissionError') {
    res.status(403).json( { errors: ['403 - You don\'t have permission for that']});
  } else if (error.name === 'ValidationError') {
    errors = Object.values(error.errors).map(validationError => validationError.message.replace(/^Path `(.+)`(.+)/i, '$1$2'))
    res.status(422).json({ errors });
  } else if(error.name === 'CastError') {
    res.status(422).json({ errors: [error.message] });
  } else if (error.name === 'NotFoundError') {
    res.status(404).json({ errors: ['404 - 🐾 you lost track!'] });
  } else {
    console.error(error);
    res.status(500).json( { errors: ['500 - Ooops we broke the bone.']});
  }

  res.end();
}

module.exports.validateUri = (uri) => {
  const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator

    return !!pattern.test(uri);
}

module.exports.sanitize = (dirty, user = {}, options = {}) => {
  // exclude admins from sanitization
  if(user.permissions && user.permissions.indexOf('admin') !== -1)
    return dirty;

  const defaults = {
    allowedTags: [ 'b', 'i', 'u', 'em', 'strong', 'a', 'br', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'span', 'div', 'table', 'tbody', 'tr', 'td', 'ul', 'li', 'img', 'pre' ],
    allowedAttributes: {
      'a': [ 'href' ],
      'img': [ 'src' ],
      'span': [ 'style' ],
      'div': [ 'style' ]
    },
    allowedSchemes: [ 'data', 'http', 'https' ]
  };

  defaultedOptions = assign(defaults, options);

  return sanitize(dirty, defaultedOptions);
}

module.exports.sanitizeRegex = (dirty) => {
  return dirty.replace(/[^0-9A-Za-z\_\.]/i, '')
}
