const fs = require('fs');

const Thread = require('@/models/messageThread.js');
const Message = require('@/models/message.js');
const User = require('@/models/user.js');

const mailSettings = require('@/config/mail.js').settings;
const { APP_URI } = require('@/config/app.js').constants;

const MailRecipient = require("mailersend").Recipient;
const EmailParams = require("mailersend").EmailParams;
const MailerSend = require("mailersend");

// EMail Setup

let mailTemplate;

try {
  mailTemplate = fs.readFileSync('views/mail.htm', 'utf8')
} catch (error) {
  console.error(error)
}

const mailersend = new MailerSend({
  api_key: mailSettings.api_key,
});

// Functions

const getSystemUser = async () => {
  const systemUser = await User.findOne({username: 'System'});

  return systemUser;
}

const sendMail = async function(title, content, cta, to) {
  if(!to.email)
    return;

  let html = mailTemplate;

  html = html.replace('{content}', content);
  html = html.replace('{cta_to}', cta.to);
  html = html.replace('{cta_text}', cta.text);

  const emailParams = new EmailParams()
    .setFrom(mailSettings.from)
    .setFromName(mailSettings.from_name)
    .setRecipients([new MailRecipient(to.email)])
    .setSubject(title)
    .setHtml(html)
    .setText(content);

  try {
    const res = await mailersend.send(emailParams);
  } catch (error) {
    console.log(error)
  }
}

const sendSystemMail = async function(title, content, to, settingsKey, options) {
  const toUser = await User.findById(to).select('_id settings email');

  if(toUser && toUser.settings && toUser.settings[settingsKey]) {
    try {
      let emailTitle = title;
      let emailContent = content;
      let cta = {
        to: APP_URI,
        text: 'Visit our website'
      }

      switch(settingsKey) {
        case 'emailAnnouncements':
          emailTitle = 'A new anouncement has been made!'
          cta.to += '/news'
          cta.text = 'Go to news'
          break;
        case 'emailMentions':
          cta.to = options.link
          cta.text = 'Go to thread'
          break;
        case 'emailSubscribedThread':
          cta.to = options.link
          cta.text = 'Go to thread'
          break;
        case 'emailSubmissionComments':
          cta.to = options.link
          cta.text = 'Go to submission'
          break;
        case 'emailPMs':
          cta.to += '/messages'
          cta.text = 'Go to private messages'
          break;
      }

      await sendMail(emailTitle, emailContent, cta, toUser)
    } catch (error) {
      console.log(error)
    }
  }
}

const sendSystemMessage = async function(title, content, to, settingsKey=null, options={}) {
  const systemUser = await getSystemUser();
  const thread = new Thread();
  const message = new Message();

  message.author = systemUser._id;
  message.content = content;

  thread.from = systemUser._id;
  thread.title = title;
  thread.to = to;
  thread.type = 'alert';

  await thread.save();

  message.thread = thread;

  if(settingsKey) {
    try {
      await sendSystemMail(title, content, to, settingsKey, options)
    } catch (error) {
      console.log(error)
    }
  }

  return await message.save();
}

module.exports.sendSystemMessage = sendSystemMessage
module.exports.sendSystemMail = sendSystemMail
module.exports.sendMail = sendMail

module.exports.mention = async function(thread, user) {
  const title = 'Someone mentioned you in a post';

  const link = APP_URI + '/forums/' + thread.board._id + '/threads/' + thread._id;

  let message = 'You were mentioned mentioned in '
                message += `<a href="${link}" target="_blank">`
                message += thread.title
                message += '</a>';

  return await sendSystemMessage(title, message, user, 'emailMentions', {link});
}
