const UserSection = require('@/models/userSection.js');

module.exports.getOtherSection = async function(user) {
  const otherUserSectionName = 'Other';

  let otherUserSection = await UserSection.findOne({ name : otherUserSectionName, user});

  if(!otherUserSection) {
    otherUserSection = new UserSection();
    otherUserSection.name = otherUserSectionName;
    otherUserSection.user = user;
    await otherUserSection.save();
  }

  return otherUserSection
}
