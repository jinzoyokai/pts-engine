const moment = require('moment')

const { getOtherSection } = require('@/mixins/users.js');

const Companion = require('@/models/companion.js');
const Item = require('@/models/item.js');
const User = require('@/models/user.js');
const UserItem = require('@/models/userItem.js');
const CharacterCompanion = require('@/models/characterCompanion.js');
const TradeInDone = require('@/models/tradeIns/tradeInDone.js');

module.exports.finishTradeIn = async function(tradeIn, user) {
  const tradeInDone = new TradeInDone()
  tradeInDone.tradeIn = tradeIn
  tradeInDone.user = user

  await tradeInDone.save()

  return tradeInDone
}

module.exports.checkTradeInLimit = async function(tradeIn, user) {
  if(
    !tradeIn.limitType
    || !tradeIn.limitValue
  )
    return;

  let timestamp = moment().tz('America/Toronto');

  switch (tradeIn.limitType) {
    case 'daily':
      timestamp = timestamp.startOf('day')
      break;
    case 'weekly':
      timestamp = timestamp.startOf('week')
      break;
    case 'monthly':
      timestamp = timestamp.startOf('month')
      break;
    default:
      return;
  }

  timestamp = timestamp.toDate();


  const count = await TradeInDone.count({
    tradeIn,
    user,
    createdAt: {
      $gte: timestamp,
    }
  })

  return count >= tradeIn.limitValue;
}



module.exports.handleRewardSet = async function(reward, user, userItems) {
  const otherSection = await getOtherSection(user);

  await Promise.all(userItems.map((userItem) => {
    return userItem.remove();
  }));
  if(reward.items)
    await Promise.all(reward.items.map((item) => {
      const userItem = new UserItem();

      userItem.user = user;
      userItem.item = item;

      return userItem.save();
    }));

  if(reward.companions)
    await Promise.all(reward.companions.map((companion) => {
      const characterCompanion = new CharacterCompanion();

      characterCompanion.user = user;
      characterCompanion.companion = companion;
      characterCompanion.section = otherSection;

      return characterCompanion.save();
    }));

  let randomItem;

  if(reward.randomItem) {
    const itemCount = await Item.estimatedDocumentCount();
    const itemRandom = Math.floor(Math.random() * itemCount)

    const item = await Item.findOne().skip(itemRandom);

    randomItem = new UserItem();

    randomItem.user = user;
    randomItem.item = item;

    await randomItem.save();

    randomItem = randomItem.toJSON()
  }

  let randomCompanion;

  if(reward.randomCompanion) {
    const companionCount = await Companion.estimatedDocumentCount();
    const companionRandom = Math.floor(Math.random() * companionCount)

    const companion = await Companion.findOne().skip(companionRandom);

    randomCompanion = new CharacterCompanion();

    randomCompanion.user = user;
    randomCompanion.companion = companion;
    randomCompanion.section = otherSection;

    await randomCompanion.save();

    randomCompanion = randomCompanion.toJSON()
  }

  /* CURRENCY */
  let newBalance;
  if(
    reward.currency > 0
  ) {
    const updatedUser = {};

    newBalance = user.currency + reward.currency;

    updatedUser.currency = newBalance;
    await User.updateOne({ _id: user._id }, updatedUser);
  }



  return {
    ...reward.toJSON(),
    randomItem,
    randomCompanion,
    newBalance
  }
}
