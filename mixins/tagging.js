const { uniq } = require('lodash');
const { handleValidationError, sanitize } = require('@/mixins/validation.js');
const { mention } = require('@/mixins/messaging.js');

const User = require('@/models/user.js');


module.exports.tagUsers = async function(body, thread = null) {
  body = sanitize(body);

  const m = body.match(/\@[A-a-z0-9\_\-]+(\.[a-z0-9\_\-]+)?(<\/a>)?/gi);

  const usernames = uniq(m).filter(username => username.substr(-4) !== '</a>')
                 .map(username => username.replace('@', ''));

  for (let username of usernames) {
    try {
      const usernameExp =  new RegExp('^' + username.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&') + '$', 'i')
      const user = await User.findOne({'username' : usernameExp});
      const mentionString = '@' + username;

      const link = '<a href="/u/' + user.username + '" target="_blank">' + mentionString + '</a>';

      body = body.replace(new RegExp(mentionString, 'gi'), link)

      if(!!thread)
        await mention(thread, user);
    } catch (e) {
      console.error(e);
    }
  }

  return body;
}
