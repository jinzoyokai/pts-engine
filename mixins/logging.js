const Log = require('@/models/log.js');
const Item = require('@/models/item.js');
const Companion = require('@/models/companion.js');
const User = require('@/models/user.js');

const { currencies } = require('@/common/currencies.js');

module.exports.log = async (model, entity, action, {_id:staff}, oldModel={}) => {
  const log = new Log();

  log.type = entity;
  log.staff = staff;

  if(entity === 'item') {
    const item = await Item.findById(model.item);

    log.user = model.user;

    if(action === 'save') {
      log.action = 'add';
      log.value = model.item;
      log.message = `Added item: "${item.name}"`;
    } else if(action === 'remove') {
      log.action = 'remove';
      log.value = model.item;
      log.message = `Removed item: "${item.name}"`;
    }
  } else if(entity === 'companion') {
    const companion = await Companion.findById(model.companion);

    log.user = model.user;

    if(action === 'save') {
      log.action = 'add';
      log.value = model.companion;
      log.message = `Added pokemon: "${companion.name}"`;
    } else if(action === 'remove') {
      log.action = 'remove';
      log.value = model.companion;
      log.message = `Removed pokemon: "${companion.name}"`;
    }
  } else if(entity === 'user') {
    const user = await User.findById(model._id);

    log.user = user;

    if(action === 'currency') {
      const currencyKey = model.currencyKey || 'currency';
      const usedCurrency = currencies.find(({key}) => key === currencyKey)

      let difference = parseInt(model[currencyKey]) - parseInt(oldModel[currencyKey]);

      if(difference === 0) {
          return;
      } else if(difference < 0) {
        log.action = 'remove';
        difference = difference * -1;
        log.message = `Removed ${difference} ${usedCurrency.symbol}`;
      } else {
        log.action = 'add';
        log.message = `Added ${difference} ${usedCurrency.symbol}`;
      }

      log.value = difference;
    }
  } else if(entity === 'game') {
    const item = await Item.findById(model.item);

    log.user = model.user;

    if(action === 'gacha') {
      log.action = 'gacha';
      log.value = model._id;

      log.message = `Played Gacha. (Won ${model.amount}x ${model.item.name})`;
    }
  } else if(entity === 'shopItem') {
    const item = await Item.findById(model.item);

    log.user = staff;

    if(action === 'save') {
      log.action = 'buy';
      log.value = model._id;

      log.message = `Bought item "${item.name}" for ${model.price} ${model.currency.replace('_', ' ')}`;
    }
  } else if(entity === 'shopCompanion') {
    const companion = await Companion.findById(model.companion);

    log.user = staff;

    if(action === 'save') {
      log.action = 'buy';
      log.value = model._id;

      log.message = `Bought pokemon "${companion.name}" for ${model.price} ${model.currency.replace('_', ' ')}`;
    }
  } else if(entity === 'trade') {
    const item = await Item.findById(model.item);

    log.user = model.user;

    if(action === 'save') {
      log.action = 'create';
      log.value = model._id;

      log.message = `Opened trade with "${model.to.username}"`;
    } else if(action === 'remove') {
      log.action = 'remove';
      log.value = model._id;

      log.message = `Deleted a trade (from: ${model.from.username}, to: ${model.to.username})`;
    } else if(action === 'accept') {
      log.action = 'accept';
      log.value = model._id;

      log.message = `Accepted a trade (from: ${model.from.username}, to: ${model.to.username})`;
    } else if(action === 'approve') {
      log.action = 'approve';
      log.value = model._id;

      log.message = `Approved a trade (from: ${model.from.username}, to: ${model.to.username})`;
    }
  } else if(entity === 'tradeIn') {
    log.user = model.user;

    if(action === 'tradeIn') {
      log.action = 'tradeIn';
      log.value = model.tradeIn._id;

      let message = 'Traded in the following items ';

      message += model.userItems.map(({item}) => item.name).join(', ')

      if(model.tradeIn.price) {
        message += ` and ${model.tradeIn.price} ${model.tradeIn.currency}`
      }

      message += ' for '

      let rewards = [];

      if(model.rewards.randomItem) {
        rewards.push(model.rewards.randomItem.item.name)
      }

      if(model.rewards.randomCompanion) {
        rewards.push(model.rewards.randomCompanion.companion.name)
      }

      if(model.rewards.items) {
        model.rewards.items.forEach(({name}) => rewards.push(name))
      }

      if(model.rewards.companions) {
        model.rewards.companions.forEach(({name}) => rewards.push(name))
      }

      if(model.rewards.currency) {
        rewards.push(`${model.rewards.currency} PD`)
      }

      message += rewards.join(', ')

      log.message = message;
    } else if(action === 'tradeInStart') {
      log.action = 'start';
      log.value = model.tradeIn._id;

      let message = 'Started a trade in with the following items ';

      message += model.userItems.map(({item}) => item.name).join(', ')

      if(model.tradeIn.price) {
        message += ` and ${model.tradeIn.price} ${model.tradeIn.currency}`
      }

      log.message = message;
    } else if(action === 'tradeInEnd') {
      log.action = 'end';
      log.value = model.tradeIn._id;

      let message = 'Ended a trade in and selected the following reward ';

      let rewards = [];

      if(model.rewards.randomItem) {
        rewards.push(model.rewards.randomItem.item.name)
      }

      if(model.rewards.randomCompanion) {
        rewards.push(model.rewards.randomCompanion.companion.name)
      }

      if(model.rewards.items) {
        model.rewards.items.forEach(({name}) => rewards.push(name))
      }

      if(model.rewards.companions) {
        model.rewards.companions.forEach(({name}) => rewards.push(name))
      }

      if(model.rewards.currency) {
        rewards.push(`${model.rewards.currency} PD`)
      }

      message += rewards.join(', ')

      log.message = message;
    } else if(action === 'tradeInCancel') {
      log.action = 'cancel';
      log.value = model.tradeIn._id;

      let message = 'Canceled a trade in.';

      log.message = message;
    }
  }

  await log.save();
}
