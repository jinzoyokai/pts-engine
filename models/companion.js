const mongoose = require('mongoose');

const CharacterCompanion = require('@/models/characterCompanion.js');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  icon: {
    type: String,
    required: false
  },
  oldId: {
    type: Number,
    required: false
  },
});

s.pre('remove', async function(next) {
  await CharacterCompanion.deleteMany({ companion: this._id });
  next();
});

module.exports = mongoose.model('Companion', s);
