const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const { s3: s3Storage } = require('@/config/storage.js');
const AWS = require('aws-sdk');
const Jimp = require('jimp');

const Schema = mongoose.Schema;

let s = new Schema({
  image: {
    type: String,
    required: true
  },
  thumbnail: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false,
    default: ' ',
  },
  status: {
    type: String,
    required: true,
    default: 'submitted'
  },
  hidden: {
    type: Boolean,
    required: false,
    default: false,
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('comments', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'submission',
    justOne: false,
});

s.pre('save', (next) => {
  this.updatedAt = Date.now();
  next();
});

s.methods.generateThumbnail = async function() {
  const s3 = new AWS.S3(s3Storage.config);

  return new Promise(async (resolve, reject) => {
    try {
      const image = await Jimp.read(this.image)
      image.resize(Jimp.AUTO, 240);

      const thumbnailBuffer = await image.getBufferAsync(Jimp.AUTO);
      let filename = this.image.match(/\.com\/(.+)$/i)[1]
      filename = `thumbnail-${filename}`;

      const params = {
         Bucket: s3Storage.bucket, // pass your bucket name
         Key: filename, // file will be saved as testBucket/contacts.csv
         Body: thumbnailBuffer,
         ACL: 'public-read'
       };
       const result = await s3.upload(params).promise()

       this.thumbnail = result.Location;

       await this.save();
       resolve();
    } catch (error) {
      reject(error);
    }
  })
}

s.plugin(mongoosePaginate);

module.exports = mongoose.model('Submission', s);
