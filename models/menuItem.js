const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  label: {
    type: String,
    required: false
  },
  url: {
    type: String,
    required: false
  },
  icon: {
    type: String,
    required: false
  },
  authOnly: {
    type: Boolean,
    required: false,
    default: false,
  },
  order: {
    type: Number,
    required: false,
    default: 0,
  },
  children: {
    type: [Object],
    required: false,
  },
});

module.exports = mongoose.model('MenuItem', s);
