const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  companion: {
    type: mongoose.ObjectId,
    ref: 'Companion',
    required: true
  },
  pokeball: {
    type: mongoose.ObjectId,
    ref: 'Item',
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: false
  },
  image: {
    type: String,
    required: true
  },
  urlMatch: {
    type: String,
    required: true
  },
  status: {
    type: String,
    required: true,
    default: 'scheduled'
  },
  schedule: {
    type: Date,
    required: true
  },
  caughtAt: {
    type: Date,
    required: false,
  },
});

module.exports = mongoose.model('QuickCatchEvent', s);
