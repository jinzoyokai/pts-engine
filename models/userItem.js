const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  item: {
    type: mongoose.ObjectId,
    ref: 'Item',
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: false
  },
  character: {
    type: mongoose.ObjectId,
    ref: 'Character',
    required: false
  },
  trade: {
    type: mongoose.ObjectId,
    ref: 'Trade',
    required: false
  },
  soulbound: {
    type: Boolean,
    required: false,
    default: false,
  },
});

module.exports = mongoose.model('UserItem', s);
