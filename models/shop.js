const mongoose = require('mongoose');

const ShopItem = require('@/models/shopItem.js');
const ShopCompanion = require('@/models/shopCompanion.js');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  slug: {
    type: String,
    unique: true,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  icon: {
    type: String,
    required: false
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('inventory', {
    ref: 'ShopItem',
    localField: '_id',
    foreignField: 'shop',
    justOne: false,
});

s.virtual('companionInventory', {
    ref: 'ShopCompanion',
    localField: '_id',
    foreignField: 'shop',
    justOne: false,
});

s.pre('remove', async function(next) {
  await ShopItem.deleteMany({ shop: this._id });
  await ShopCompanion.deleteMany({ shop: this._id });
  next();
});

module.exports = mongoose.model('Shop', s);
