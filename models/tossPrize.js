const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  pokeball: {
    type: mongoose.ObjectId,
    ref: 'Item',
    required: true
  },
  items: {
    type: [mongoose.ObjectId],
    ref: 'Item',
    required: true
  },
  pokemon: {
    type: [mongoose.ObjectId],
    ref: 'Companion',
    required: true
  },
  weight: {
    type: Number,
    required: true
  },
});

module.exports = mongoose.model('TossPrize', s);
