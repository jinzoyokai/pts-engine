const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  stock: {
    type: Number,
    required: false,
    default: 0,
  },
  unlimitedStock: {
    type: Boolean,
    required: false,
    default: false,
  },
  currency: {
    type: String,
    required: false,
    default: 'currency',
  },
  price: {
    type: Number,
    required: false,
    default: 0,
  },
  companion: {
    type: mongoose.ObjectId,
    ref: 'Companion',
    required: true
  },
  shop: {
    type: mongoose.ObjectId,
    ref: 'Shop',
    required: true
  },
});

module.exports = mongoose.model('ShopCompanion', s);
