const mongoose = require('mongoose'),
      { omit } = require('lodash');

const mongooseHidden = require('mongoose-hidden')(),
      mongooseDelete = require('mongoose-delete');


const File = require('@/models/file.js'),
      Character = require('@/models/character.js'),
      UserItem = require('@/models/userItem.js'),
      Submission = require('@/models/submission.js'),
      TradeInDone = require('@/models/tradeIns/tradeInDone.js');

const Schema = mongoose.Schema;

let s = new Schema({
  username: {
    type: String,
    unique: true,
    required: true,
  },
  email: {
    type: String,
    required: false,
    match: [/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/, 'Please provide a valid email address']
  },
  currency: {
    type: Number,
    required: false,
    default: 0,
  },
  thorn_reputation_points: {
    type: Number,
    required: false,
    default: 0,
  },
  aurora_faction_points: {
    type: Number,
    required: false,
    default: 0,
  },
  eventide_faction_points: {
    type: Number,
    required: false,
    default: 0,
  },
  casino_tickets: {
    type: Number,
    required: false,
    default: 0,
  },
  casino_chips: {
    type: Number,
    required: false,
    default: 0,
  },
  pledge_points:{
    type: Number,
    required: false,
    default: 0,
  },
  dA: {
    type: String,
    required: false
  },
  discord: {
    type: String,
    required: false
  },
  bio: {
    type: String,
    required: false
  },
  icon: {
    type: String,
    required: false
  },
  rank: {
    type: mongoose.ObjectId,
    ref: 'Rank',
    required: false
  },
  achievements: {
    type: String,
    required: false,
  },
  permissions: {
    type: Array,
    required: true
  },
  activityCheck: {
    type: String,
    required: false,
  },
  activityCheckDate: {
    type: Date,
    required: false,
  },
  modActivityCheck: {
    type: String,
    required: false,
  },
  modActivityCheckDate: {
    type: Date,
    required: false,
  },
  hiatus: {
    type: Boolean,
    required: false,
    default: false,
  },
  news: {
    type: mongoose.ObjectId,
    ref: 'Content',
    required: false
  },
  hiatusStartedAt: {
    type: Date,
    required: false,
  },
  password: {
    type: String,
    required: false,
    hideJSON: true,
  },
  hasTemporaryPassword: {
    type: Boolean,
    required: false,
    default: false,
  },
  token: {
    type: String,
    required: false,
  },
  oldId: {
    type: Number,
    required: false,
  },
  role: {
    type: Number,
    required: false,
  },
  deletedAt: {
    type: Date,
    required: false,
  },
  settings: {
    type: Object,
    required: true,
    default: {
      emailAnnouncements: true,
      emailMentions: false,
      emailSubscribedThread: false,
      emailSubmissionComments: false,
      emailPMs: true,
    }
  }
},
{
  toJSON: {
    virtuals: true
  }
});

// s.path('email').index({ unique: true });
s.virtual('files', {
    ref: 'File',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.virtual('characters', {
    ref: 'Character',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.virtual('companions', {
    ref: 'CharacterCompanion',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.virtual('sections', {
    ref: 'UserSection',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.virtual('inventory', {
    ref: 'UserItem',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.virtual('submissions', {
    ref: 'Submission',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.virtual('tradeInsDone', {
    ref: 'TradeInDone',
    localField: '_id',
    foreignField: 'user',
    justOne: false,
});

s.pre('remove', async function(next) {
  await Promise.all([
    File.deleteMany({ user: this._id }),
    Character.deleteMany({ user: this._id }),
    UserItem.deleteMany({ user: this._id }),
    TradeInDone.deleteMany({ user: this._id })
  ]);
  next();
});



s.plugin(mongooseHidden, { hidden: { _id: false }})
s.plugin(mongooseDelete, { deletedAt : true, overrideMethods: 'all' })

module.exports = mongoose.model('User', s);
