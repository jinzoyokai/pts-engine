const mongoose = require('mongoose');

const Character = require('@/models/character.js');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  icon: {
    type: String,
    required: false
  },
});

s.pre('remove', async function(next) {
  User.update(
    { rank: this._id },
    { rank: null },
  );
  next();
});

module.exports = mongoose.model('Rank', s);
