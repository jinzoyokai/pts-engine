const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  design: {
    type: String,
    required: false
  },
  level: {
    type: String,
    required: false
  },
  pokeball: {
    type: mongoose.ObjectId,
    ref: 'Item',
    required: false
  },
  image: {
    type: String,
    required: false
  },
  notes: {
    type: String,
    required: false
  },
  mod_notes: {
    type: String,
    required: false
  },
  team: {
    type: Boolean,
    required: false
  },
  salon: {
    type: Boolean,
    required: false
  },
  order: {
    type: Number,
    required: false,
    default: 0
  },
  colorStatus: {
    type: mongoose.ObjectId,
    ref: 'ColorStatus',
    required: false
  },
  colorStatuses: [{
    type: mongoose.ObjectId,
    ref: 'ColorStatus'
  }],
  companion: {
    type: mongoose.ObjectId,
    ref: 'Companion',
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  character: {
    type: mongoose.ObjectId,
    ref: 'Character',
    required: false
  },
  section: {
    type: mongoose.ObjectId,
    ref: 'UserSection',
    required: false
  },
  trade: {
    type: mongoose.ObjectId,
    ref: 'Trade',
    required: false
  },
  oldId: {
    type: Number,
    required: false
  },
  'old_user_id': {
    type: Number,
    required: false
  },
  'old_trainer_id': {
    type: Number,
    required: false
  },
  'old_pokemon_id': {
    type: Number,
    required: false
  },
  'orderr': {
    type: Number,
    required: false
  },
});

module.exports = mongoose.model('CharacterCompanion', s);
