const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const User = require('@/models/user');

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('companions', {
    ref: 'CharacterCompanion',
    localField: '_id',
    foreignField: 'section',
    justOne: false,
});

module.exports = mongoose.model('UserSection', s);
