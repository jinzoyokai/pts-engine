const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Thread = require('@/models/thread.js'),
      Post = require('@/models/post.js')

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  icon: {
    type: String,
    required: false
  },
  locked: {
    type: Boolean,
    required: false,
    default: false,
  },
  order: {
    type: Number,
    required: false,
    default: 0,
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('thread', {
    ref: 'Thread',
    localField: '_id',
    foreignField: 'board',
    justOne: false,
});

s.virtual('posts', {
    ref: 'Post',
    localField: '_id',
    foreignField: 'board',
    justOne: false,
});

s.pre('remove', async function(next) {
  await Promise.all([
    Thread.deleteMany({ board: this._id }),
    Post.deleteMany({ board: this._id }),
  ]);
  next();
});

module.exports = mongoose.model('Board', s);
