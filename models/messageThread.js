const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;

let s = new Schema({
  from: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true,
  },
  to: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true,
  },
  fromRead: {
    type: Boolean,
    required: false,
    default: true,
  },
  toRead: {
    type: Boolean,
    required: false,
    default: false,
  },
  type: {
    type: String,
    required: false,
    default: 'message',
  },
  title: {
    type: String,
    required: false,
    default: 'No title',
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('messages', {
    ref: 'Message',
    localField: '_id',
    foreignField: 'thread',
    justOne: false,
});

s.pre('save', (next) => {
  this.updatedAt = Date.now();
  next();
});

s.plugin(mongoosePaginate);

module.exports = mongoose.model('messageThread', s);
