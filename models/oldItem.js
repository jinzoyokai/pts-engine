const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  'old_user_id': {
    type: Number,
    required: false
  },
});

module.exports = mongoose.model('OldItem', s);
