const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  type: {
    type: String,
    required: true
  },
  action: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: false,
  },
  message: {
    type: String,
    required: false,
    default: 1,
  },
  staff: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: false
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: false
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});

module.exports = mongoose.model('Log', s);
