const mongoose = require('mongoose'),
      fs = require('fs');

const Schema = mongoose.Schema;

let s = new Schema({
  key: {
    type: String,
    required: false
  },
  description: {
    type: String,
    required: false
  },
  file: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  character: {
    type: mongoose.ObjectId,
    ref: 'Character',
    required: false
  },
});

s.pre('remove', { document: true }, function() {
  fs.unlink(this.file, () => {
    // file deleted
  });
});


module.exports = mongoose.model('File', s);
