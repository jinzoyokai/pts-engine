const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  category: {
    type: String,
    required: true
  },
  message: {
    type: String,
    required: true
  },
  character: {
    type: mongoose.ObjectId,
    ref: 'Character',
    required: true
  },
  items: [{
      type: Schema.ObjectId,
      ref: 'UserItem'
  }],
  books: [{
      type: Schema.ObjectId,
      ref: 'CharacterBook'
  }],
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});

module.exports = mongoose.model('TravelTrackerEntry', s);
