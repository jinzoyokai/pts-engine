const mongoose = require('mongoose');

const UserItem = require('@/models/userItem.js');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  rarity: {
    type: String,
    required: false
  },
  value: {
    type: Number,
    required: false
  },
  modifier: {
    type: Number,
    required: false
  },
  icon: {
    type: String,
    required: false
  },
  tags: {
    type: Array,
    required: false
  },
});

s.pre('remove', async function(next) {
  await UserItem.deleteMany({ item: this._id });
  next();
});

module.exports = mongoose.model('Item', s);
