const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  item: {
    type: mongoose.ObjectId,
    ref: 'Item',
    required: true
  },
  weight: {
    type: Number,
    required: true
  },
  amount: {
    type: Number,
    required: true
  },
});

module.exports = mongoose.model('GachaItem', s);
