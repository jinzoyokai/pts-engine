const mongoose = require('mongoose');

const CharacterCompanion = require('@/models/characterCompanion.js');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  css: {
    type: String,
    required: true
  },
});

s.pre('remove', async function(next) {
  await CharacterCompanion.update(
    { colorStatus: this._id },
    { colorStatus: null },
  );
  next();
});

module.exports = mongoose.model('ColorStatus', s);
