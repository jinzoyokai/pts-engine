const mongoose = require('mongoose'),
      mongoosePaginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;

let s = new Schema({
  slug: {
    type: String,
    unique: true,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  category: {
    type: String,
    required: true
  },
  hasComments: {
    type: Boolean,
    required: true
  },
  showInActivitiesTab: {
    type: Boolean,
    required: false,
    default: false,
  },
  showInNav: {
    type: Boolean,
    required: false,
    default: false,
  },
  navLabel: {
    type: String,
    required: false,
  },
  notifyUsers: {
    type: Boolean,
    required: false,
    default: false,
  },
  notificationMessage: {
    type: String,
    required: false,
  },
  body: {
    type: String,
    required: false
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('comments', {
    ref: 'Comment',
    localField: '_id',
    foreignField: 'content',
    justOne: false,
});

s.pre('save', (next) => {
  this.updatedAt = Date.now();
  next();
});

s.plugin(mongoosePaginate);

module.exports = mongoose.model('Content', s);
