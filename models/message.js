const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  author: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  thread: {
    type: mongoose.ObjectId,
    ref: 'Thread',
    required: true
  },
  content: {
    type: String,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});

module.exports = mongoose.model('Message', s);
