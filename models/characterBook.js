const { sum } = require('lodash');

const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Rank = require('@/models/rank');

let s = new Schema({
  book: {
    type: mongoose.ObjectId,
    ref: 'Book',
    required: true
  },
  character: {
    type: mongoose.ObjectId,
    ref: 'Character',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});

s.post('save', async (doc, next) => {
  const Character = require('@/models/character');

  const [ranks, character] = await Promise.all([
    Rank.find(),
    await Character.findById(doc.character).populate({
        path: 'books',
        populate : {
          path : 'book'
        }
    })
  ]);

  if(!!character.books) {
    const bookValues = character.books.map(({book}) => {
      return book.value;
    });

    const totalValue = bookValues ? sum(bookValues) : 0;

    let characterRank;

    ranks.forEach((rank) => {
      if(
        !!rank.value
        && rank.value <= totalValue
        && (
          !characterRank
          || rank.value > characterRank.value
        )
      )
        characterRank = rank;
    });

    if(!!characterRank)
      await Character.updateOne({ _id: character._id }, {
        rank: characterRank._id
      });
  }
});


module.exports = mongoose.model('CharacterBook', s);
