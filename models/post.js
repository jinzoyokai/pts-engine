const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const Schema = mongoose.Schema;

const Post = require('@/models/post.js')

let s = new Schema({
  body: {
    type: String,
    required: true
  },
  board: {
    type: mongoose.ObjectId,
    ref: 'Board',
    required: true
  },
  thread: {
    type: mongoose.ObjectId,
    ref: 'Thread',
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});

s.plugin(mongoosePaginate);

s.pre('save', (next) => {
  this.updatedAt = Date.now();
  next();
});

module.exports = mongoose.model('Post', s);
