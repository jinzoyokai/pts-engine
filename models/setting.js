const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  key: {
    type: String,
    unique: true,
    required: true
  },
  label: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  group: {
    type: String,
    required: false
  },
});

module.exports = mongoose.model('Setting', s);
