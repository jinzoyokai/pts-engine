const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  label: {
    type: String,
    required: true
  },
  group: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  value: {
    type: String,
    required: true
  },
});

module.exports = mongoose.model('Rate', s);
