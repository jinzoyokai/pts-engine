const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  body: {
    type: String,
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  content: {
    type: mongoose.ObjectId,
    ref: 'Content',
    required: false
  },
  submission: {
    type: mongoose.ObjectId,
    ref: 'Submission',
    required: false
  },
  parent: {
    type: mongoose.ObjectId,
    ref: 'Comment',
    required: false
  },
  softDeleted: {
    type: Date,
    required: false
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});
s.pre('save', (next) => {
  this.updatedAt = Date.now();
  next();
});

module.exports = mongoose.model('Comment', s);
