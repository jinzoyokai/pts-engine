const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
const mongooseHidden = require('mongoose-hidden')();

const Schema = mongoose.Schema;

const Post = require('@/models/post.js')

let s = new Schema({
  title: {
    type: String,
    required: true
  },
  locked: {
    type: Boolean,
    required: false,
    default: false,
  },
  sticky: {
    type: Boolean,
    required: false,
    default: false,
  },
  board: {
    type: mongoose.ObjectId,
    ref: 'Board',
    required: true
  },
  subscribers: {
    type: [mongoose.ObjectId],
    ref: 'User',
    required: false,
    hide: true,
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('posts', {
    ref: 'Post',
    localField: '_id',
    foreignField: 'thread',
    justOne: false,
});

s.plugin(mongoosePaginate);
s.plugin(mongooseHidden, { hidden: { _id: false }});

s.pre('remove', async function(next) {
  await Post.deleteMany({ thread: this._id });
  next();
});

module.exports = mongoose.model('Thread', s);
