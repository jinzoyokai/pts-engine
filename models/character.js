const mongoose = require('mongoose');

const File = require('@/models/file.js'),
      CharacterCompanion = require('@/models/characterCompanion.js');

const { getOtherSection } = require('@/mixins/users.js');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  job: {
    type: String,
    required: false
  },
  gender: {
    type: String,
    required: false
  },
  specialty: {
    type: String,
    required: false
  },
  pronouns: {
    type: String,
    required: false
  },
  nature: {
    type: String,
    required: false
  },
  status: {
    type: String,
    required: false
  },
  teams: {
    type: [String],
    required: false
  },
  profile: {
    type: String,
    required: false
  },
  profileImage: {
    type: String,
    required: false
  },
  design: {
    type: String,
    required: false
  },
  adminNotes: {
    type: String,
    required: false
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  oldId: {
    type: Number,
    required: false
  },
  oldTeam: {
    type: Number,
    required: false
  },
  'old_user_id': {
    type: Number,
    required: false
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('books', {
    ref: 'CharacterBook',
    localField: '_id',
    foreignField: 'character',
    justOne: false,
});

s.virtual('companions', {
    ref: 'CharacterCompanion',
    localField: '_id',
    foreignField: 'character',
    justOne: false,
});

s.virtual('files', {
    ref: 'File',
    localField: '_id',
    foreignField: 'character',
    justOne: false,
});

s.virtual('inventory', {
    ref: 'UserItem',
    localField: '_id',
    foreignField: 'character',
    justOne: false,
});

s.pre('remove', async function(next) {
  const otherSection = await getOtherSection(this.user);

  await Promise.all([
    File.deleteMany({ character: this._id }),
    CharacterCompanion.updateMany(
      { character: this._id },
      { character: null, section: otherSection._id }
    )
  ]);
  next();
});

module.exports = mongoose.model('Character', s);
