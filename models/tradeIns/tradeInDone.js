const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  tradeIn: {
    type: mongoose.ObjectId,
    ref: 'TradeIn',
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
});

module.exports = mongoose.model('TradeInDone', s);
