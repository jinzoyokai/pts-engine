const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  tradeIn: {
    type: mongoose.ObjectId,
    ref: 'TradeIn',
    required: true
  },
  user: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  items: [
    {
      type: mongoose.ObjectId,
      ref: 'UserItem',
      required: true
    }
  ],
  rewards: [
    {
      weight: {
        type: Number,
        required: true
      },
      items: [{
        type: mongoose.ObjectId,
        ref: 'Item',
        required: false
      }],
      companions: [{
        type: mongoose.ObjectId,
        ref: 'Companion',
        required: false
      }],
      randomItem: {
        type: Boolean,
        required: false,
        default: false
      },
      randomCompanion: {
        type: Boolean,
        required: false,
        default: false
      },
      currency: {
        type: Number,
        required: false,
        default: 0
      },
    }
  ],
});

module.exports = mongoose.model('TradeInPool', s);
