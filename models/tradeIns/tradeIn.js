const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  name: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: false
  },
  slug: {
    type: String,
    required: true,
    unique: true
  },
  price: {
    type: Number,
    required: false,
    default: 0,
  },
  currency: {
    type: String,
    required: false,
    default: 'currency',
  },
  numberOfItemsRequired: {
    type: Number,
    required: true,
    default: 1,
  },
  showChoices: {
    type: Boolean,
    required: false,
    default: false,
  },
  numberOfChoices: {
    type: Number,
    required: false,
    default: false,
  },
  isCancelable: {
    type: Boolean,
    required: false,
    default: true,
  },
  isLimited: {
    type: Boolean,
    required: false,
    default: false,
  },
  limitValue: {
    type: Number,
    required: false,
    default: 1,
  },
  limitType: {
    type: String,
    required: false,
    default: 'weekly',
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('conditions', {
    ref: 'TradeInCondition',
    localField: '_id',
    foreignField: 'tradeIn',
    justOne: false,
});

s.virtual('tradeInsDone', {
    ref: 'TradeInDone',
    localField: '_id',
    foreignField: 'tradeIn',
    justOne: false,
});

module.exports = mongoose.model('TradeIn', s);
