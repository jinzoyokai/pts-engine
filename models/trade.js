const mongoose = require('mongoose');

const Schema = mongoose.Schema;

let s = new Schema({
  from: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  to: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: true
  },
  comment: {
    type: String,
    required: false
  },
  fromCurrency: {
    type: Number,
    required: false,
    default: 0,
  },
  toCurrency: {
    type: Number,
    required: false,
    default: 0,
  },
  fromItems: [{
    type: mongoose.ObjectId,
    ref: 'UserItem',
  }],
  toItems: [{
    type: mongoose.ObjectId,
    ref: 'UserItem',
  }],
  fromCompanions: [{
    type: mongoose.ObjectId,
    ref: 'CharacterCompanion',
  }],
  toCompanions: [{
    type: mongoose.ObjectId,
    ref: 'CharacterCompanion',
  }],
  accepted: {
    type: Boolean,
    required: false,
    default: false,
  },
  approved: {
    type: Boolean,
    required: false,
    default: false,
  },
  approvedBy: {
    type: mongoose.ObjectId,
    ref: 'User',
    required: false
  },
  createdAt: {
    type: Date,
    default: Date.now,
    required: true
  },
  updatedAt: {
    type: Date,
    default: Date.now,
    required: true
  },
},
{
  toJSON: {
    virtuals: true
  }
});

s.virtual('items', {
    ref: 'UserItem',
    localField: '_id',
    foreignField: 'trade',
    justOne: false,
});

s.virtual('companions', {
    ref: 'CharacterCompanion',
    localField: '_id',
    foreignField: 'trade',
    justOne: false,
});

s.pre('save', (next) => {
  this.updatedAt = Date.now();
  next();
});

module.exports = mongoose.model('Trade', s);
