const Post = require('@/models/post.js');

const Policy = require('@/policies/policy.js');

class PostPolicy extends Policy {
   constructor(user, model = null) {
     super(user, model);

     this.fields = Object.keys(Post.schema.paths);
   }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = ['body'];
     }

     if(
       this.user
       && this.user.permissions.indexOf('moderate') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {
     if(
       this.isUser()
       || (
         this.user
         && this.user.permissions.indexOf('moderate') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = PostPolicy;
