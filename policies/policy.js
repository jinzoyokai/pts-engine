class Policy {
  constructor(user, model) {
    this.user = user;
    this.model = model;
    this._fields = [];
  }
  get fields() {
    return this._fields;
  }
  set fields(arr) {
    this._fields = arr;
  }
  isUser() {
    if(!this.model)
      return false;

    return !!this.model.user
           && !!this.user
           && (String(this.model.user) === String(this.user._id));
  }
  read() {
    return null;
  }
  write() { }
  canRead() {
    return true;
  }
  canWrite() {
    if(this.isUser())
      return true;

    return false;
  }
}

module.exports = Policy;
