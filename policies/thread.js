const Thread = require('@/models/thread.js');

const Policy = require('@/policies/policy.js');

class ThreadPolicy extends Policy {
   constructor(user, model = null) {
     super(user, model);

     this.fields = Object.keys(Thread.schema.paths);
   }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = ['title'];
     }

     if(
       this.user
       && this.user.permissions.indexOf('moderate') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {
     if(
       (
         this.isUser()
         && !this.model.locked
       )
       || (
         this.user
         && this.user.permissions.indexOf('moderate') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = ThreadPolicy;
