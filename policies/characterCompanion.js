const { xor } = require('lodash');

const CharacterCompanion = require('@/models/characterCompanion.js');

const Policy = require('@/policies/policy.js');

class CharacterCompanionPolicy extends Policy {
    constructor(user, model = null) {
      super(user, model);

      this.fields = Object.keys(CharacterCompanion.schema.paths);
    }
    read() {
      return this.fields;
    }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = [
         'user',
         'character',
         'salon',
         'section',
         'order',
         'notes',
         'orderr',
         'team',
         'trade'
       ];
     }

     if(
       this.user
       && this.user.permissions.indexOf('can_edit_companions') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {

     if(
       (
         this.isUser()
         && !this.model.trade
       )
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_companions') !== -1
       )
     )
       return true;


     return false;
   }
}

module.exports = CharacterCompanionPolicy;
