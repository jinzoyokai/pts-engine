const Comment = require('@/models/comment.js');

const Policy = require('@/policies/policy.js');

class CommentPolicy extends Policy {
   constructor(user, model = null) {
     super(user, model);

     this.fields = Object.keys(Comment.schema.paths);
   }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = ['body'];
     }

     if(
       this.user
       && this.user.permissions.indexOf('moderate') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {
     if(
       this.isUser()
       || (
         this.user
         && this.user.permissions.indexOf('moderate') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = CommentPolicy;
