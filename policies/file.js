const Policy = require('@/policies/policy.js');

class FilePolicy extends Policy {
   canWrite() {
     if(
       this.isUser()
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_files') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = FilePolicy;
