const QuickCatchEvent = require('@/models/quickCatchEvent.js');

const Policy = require('@/policies/policy.js');

class QuickCatchEventPolicy extends Policy {
    constructor(user, model = null) {
      super(user, model);

      this.fields = Object.keys(QuickCatchEvent.schema.paths);
    }
    read() {
      return this.fields;
    }
   canWrite() {
     if(
       (
         this.model
         && (
           this.model.status === 'wild'
           || this.model.status === 'pityball'
         )
       )
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_quickcatchevents') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = QuickCatchEventPolicy;
