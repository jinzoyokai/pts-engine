const { without } = require('lodash');

const Character = require('@/models/character.js');

const Policy = require('@/policies/policy.js');

class CharacterPolicy extends Policy {
   constructor(user, model = null) {
     super(user, model);

     this.fields = Object.keys(Character.schema.paths);
   }
   read() {
     let fields = without(this.fields, ['adminNotes']);
     if(
       (
         this.user
         && this.user.permissions.indexOf('can_edit_characters') !== -1
       ) ||
       this.isUser()
     )
      return this.fields;

     return fields;
   }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = ['profile', 'profileImage', 'gender', 'pronouns', 'nature', 'speciality'];
     }

     if(
       this.user
       && this.user.permissions.indexOf('can_edit_characters') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {
     if(
       this.isUser()
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_characters') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = CharacterPolicy;
