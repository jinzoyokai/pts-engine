const Board = require('@/models/board.js');

const Policy = require('@/policies/policy.js');

class BoardPolicy extends Policy {
   canWrite() {
     if(
       !this.model.locked
       || (
         this.user
         && this.user.permissions.indexOf('moderate') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = BoardPolicy;
