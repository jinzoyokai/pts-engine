const Policy = require('@/policies/policy.js');

const UserItem = require('@/models/userItem.js');

class UserItemPolicy extends Policy {
  constructor(user, model = null) {
    super(user, model);

    this.fields = Object.keys(UserItem.schema.paths);
  }
  write() {
    let fields = [];

    if(this.isUser()) {
      fields = ['user'];

      if(!this.model.character)
        fields.push('character');
    }

    if(
      this.user
      && this.user.permissions.indexOf('can_edit_users') !== -1
    )
     return this.fields;

    return fields;
  }
  canRead() {
    if(
      this.isUser()
      || !!this.model.character
    )
      return true;

    return false;
  }
  canWrite() {
   if(
     (
       this.isUser()
       && !this.model.trade
     )
     || (
       this.user
       && this.user.permissions.indexOf('can_edit_users') !== -1
     )
   )
     return true;

   return false;
  }
}

module.exports = UserItemPolicy;
