const Policy = require('@/policies/policy.js');

class ItemPolicy extends Policy {
  canWrite() {
   if(
     this.user
     && this.user.permissions.indexOf('can_edit_items') !== -1
   )
     return true;

   return false;
  }
}

module.exports = ItemPolicy;
