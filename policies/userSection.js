const Policy = require('@/policies/policy.js');

const UserSection = require('@/models/userSection.js');

class UserSectionPolicy extends Policy {
  constructor(user, model = null) {
    super(user, model);

    this.fields = Object.keys(UserSection.schema.paths);
  }
  write() {
    let fields = [];

    if(this.isUser()) {
      fields = ['user'];

      if(!this.model.character)
        fields.push('character');
    }

    if(
      this.user
      && this.user.permissions.indexOf('can_edit_characters') !== -1
    )
     return this.fields;

    return fields;
  }
  canRead() {
    return true;
  }
  canWrite() {
   if(
     (
       this.isUser()
       && !this.model.trade
     )
     || (
       this.user
       && this.user.permissions.indexOf('can_edit_characters') !== -1
     )
   )
     return true;

   return false;
  }
}

module.exports = UserSectionPolicy;
