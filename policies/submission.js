const { xor } = require('lodash');

const Submission = require('@/models/submission.js');

const Policy = require('@/policies/policy.js');

class SubmissionPolicy extends Policy {
    constructor(user, model = null) {
      super(user, model);

      this.fields = Object.keys(Submission.schema.paths);
    }
    read() {
      if(
        this.user
        && (
          this.isUser()
          || this.user.permissions.indexOf('can_edit_submissions') !== -1
        )
      ) {
        return this.fields;
      }

      return xor(this.fields, [
        'points',
        'hidden',
      ])
    }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = ['hidden', 'description'];
     }

     if(
       this.user
       && this.user.permissions.indexOf('can_edit_submissions') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {
     if(
       this.isUser()
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_submissions') !== -1
       )
     )
       return true;

     return false;
   }
}

module.exports = SubmissionPolicy;
