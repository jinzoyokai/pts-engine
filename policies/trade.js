const { without } = require('lodash');

const Trade = require('@/models/trade.js');

const Policy = require('@/policies/policy.js');

class TradePolicy extends Policy {
   constructor(user, model = null) {
     super(user, model);

     this.fields = Object.keys(Trade.schema.paths);
   }
   isUser() {
     if(!this.model)
       return false;

     return !!this.model.from
            && !!this.user
            && (String(this.model.from._id) === String(this.user._id));
   }
   write() {
     let fields = [];

     if(
       !!this.user
       && this.user.permissions.indexOf('can_edit_characters') !== -1
     )
      return this.fields;

     if(
       !!this.model.from
       && !!this.user
       && String(this.model.to._id) === String(this.user._id)
     )
      return ['accepted'];

     return fields;
   }
   canWrite() {
     if(
       (
         this.user
         && String(this.model.to._id) === String(this.user._id)
       )
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_trades') !== -1
       )
     )
       return true;

     return false;
   }
   canDelete() {
     if(
       !this.model.accepted
       && !this.model.approved
       && (
         this.isUser()
         || (
           this.user
           && String(this.model.to._id) === String(this.user._id)
         )
         || (
           this.user
           && this.user.permissions.indexOf('can_edit_trades') !== -1
         )
       )
     )
       return true;

     return false;
   }
}

module.exports = TradePolicy;
