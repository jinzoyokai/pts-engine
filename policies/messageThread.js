const { without } = require('lodash');

const Policy = require('@/policies/policy.js');

class TradePolicy extends Policy {
   isUser() {
     if(!this.model)
       return false;

     return !!this.model.from
            && !!this.user
            && (String(this.model.from._id) === String(this.user._id));
   }
   isFrom() {
     if(!this.model)
       return false;

     return !!this.model.from
            && !!this.user
            && (String(this.model.from._id) === String(this.user._id));
   }
   isTo() {
     if(!this.model)
       return false;

     return !!this.model.from
            && !!this.user
            && (String(this.model.to._id) === String(this.user._id));
   }
   canWrite() {
     if(
       (
         this.user
         && String(this.model.to._id) === String(this.user._id)
       )
       || !!this.isUser()
     )
       return true;

     return false;
   }
   canRead() {
     if(
       (
         this.user
         && String(this.model.to._id) === String(this.user._id)
       )
       || !!this.isUser()
     )
       return true;

     return false;
   }
}

module.exports = TradePolicy;
