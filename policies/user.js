const User = require('@/models/user.js');

const Policy = require('@/policies/policy.js');

class UserPolicy extends Policy {
   constructor(
     user,
     model = null,
     options = {
       activityCheck: false,
       modActivityCheck: false
     }
   ) {
     super(user, model);
     this.activityCheck = options ? options.activityCheck : false;
     this.modActivityCheck = options ? options.modActivityCheck : false;
     this.fields = Object.keys(User.schema.paths);
   }
   isUser() {
     if(!this.model)
       return false;

     return !!this.model._id
            && !!this.user
            && (String(this.model._id) === String(this.user._id));
   }
   read() {
     let fields = [
        'achievements',
        'icon',
        'username',
        'bio',
        'currency',
        'inventory',
        'thorn_reputation_points',
        'aurora_faction_points',
        'eventide_faction_points',
        'casino_tickets',
        'casino_chips',
        'pledge_points'
      ];

      if(this.isUser()) {
        fields.push('activityCheck');
        fields.push('activityCheckDate');
        fields.push('hasTemporaryPassword');
        fields.push('email');
        fields.push('settings');

        if(this.user.permissions.indexOf('moderate') !== -1) {
          fields.push('modActivityCheck');
          fields.push('modActivityCheckDate');
        }
      }

     if(
      !!this.user
      && this.user.permissions.indexOf('can_edit_users') !== -1
     )
       return this.fields;

     return fields;
   }
   write() {
     let fields = [];

     if(this.isUser()) {
       fields = [
        'icon',
       'username',
       'bio',
       'hiatus',
       'password',
       'hasTemporaryPassword',
       'email',
       'settings'
     ];

       if(
        !!this.activityCheck
        && !!this.user
      ) {
        fields.push('activityCheck');
        fields.push('activityCheckDate');
      }

       if(
        !!this.modActivityCheck
        && !!this.user
        && this.user.permissions.indexOf('moderate') !== -1
       )
         fields.push('modActivityCheck');
         fields.push('modActivityCheckDate');
     }

     if(
       this.user &&
       this.user.permissions.indexOf('can_edit_users') !== -1
     )
      return this.fields;

     return fields;
   }
   canWrite() {
     if(
       this.isUser()
       || (
         this.user
         && this.user.permissions.indexOf('can_edit_users') !== -1
       )
     )
       return true;

     return false;
   }
   canReadLogs() {
     if(
       this.isUser()
       || (
         this.user
         && (
           this.user.permissions.indexOf('admin') !== -1
           || this.user.permissions.indexOf('moderate') !== -1
         )
       )
     )
       return true;

     return false;
   }
}

module.exports = UserPolicy;
